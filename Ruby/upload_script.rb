
require 'socket'
require 'timeout'

FILENAME = '../STMCubeWorkspace/ArcDetect/Release/ArcDetect.bin';

class SocketHandler
	attr_reader :device_params
	attr_reader :nick

	def initialize(target_ip, nick)
		@target_ip = target_ip
		@nick = nick

		@on_event_list = [];

		@device_params = Hash.new();

		@sock_rx_thread = Thread.new() do
			loop do
				puts "Reconnecting to #{@nick}"

				begin
					@socket = Socket.tcp(@target_ip, 22, connect_timeout: 5);	
				rescue
					sleep 3
					retry
				end
				
				Timeout.timeout(5) do
					loop do
						l = @socket.gets()
						match = /^CONNECT (.+)$/.match(l);
						next if match.nil?
						
						param_list = match[1].split(" ");
						param_list.each do |p|
							if(p =~ /(.*)=(.*)/)
								@device_params[$1] = $2 
							end
						end
						
						break;
					end
				end
					
				puts "Connected to #{@nick}!"
				
				@connected = true
				@sock_wake_thread.run();
				
				fire_event(:connected)
				
				loop do
					line = @socket.gets()
					puts "%10s:" % [@nick] + line
					fire_event(:message, data: line);
				end
			rescue Errno::ECONNRESET, Errno::EHOSTUNREACH
				@connected = false;

				sleep 1
			retry
			end
		end
		@sock_rx_thread.abort_on_exception = true

		@sock_wake_thread = Thread.new() do
			loop do
				Thread.stop until @connected

				begin
					loop do
						sleep 1
						@socket.puts "\n"
					end
				rescue Errno::ECONNRESET, Errno::EPIPE
					puts "Lost connection to #{@target_ip}"
					@connected = false;
					@sock_rx_thread.run();
				end
			end
		end

		@sock_wake_thread.abort_on_exception = true
	end

	def connected?
		return @connected
	end

	def fire_event(type, **payload)
		@on_event_list.each do |cb|
			cb.call({type: type, **payload});
		end
	end

	def send(line)
		return unless @connected
		@socket.puts line
	end

	def reset
		send("RESET")
	end

	def on_event(&block)
		@on_event_list << block;
	end
end

zethus = SocketHandler.new('172.16.32.154', 'Zethus');
amphion = SocketHandler.new('172.16.32.155', 'Amphion');

$devices = [amphion, zethus];
devices = $devices

$upload_data = Hash.new
$upload_version = File.mtime(FILENAME).to_i
$upload_file = File.read(FILENAME).bytes

FLASH_START_LOCATION = 0x08100200

def handle_bootloader(device, data)
	$upload_data[device.nick] ||= Hash.new
	dev_data = $upload_data[device.nick];

	case data[:type]
	when :connected
		puts "Device params are #{device.device_params}"
		if(device.device_params['MODE'] == 'BOOTLOADER')
			puts "Got a device in bootloader mode!"

			if(device.device_params['VERSION'].to_i != $upload_version)
				puts "Versions do not match, beginning upload..."
				dev_data[:state] = :UPLOADING
				dev_data[:offset] = -120
				device.send "[OTA]FLASH_START #{FLASH_START_LOCATION} #{$upload_file.size} #{$upload_version}"
			else
				puts "Device up to date!"
				device.send "LAUNCH_APP"
			end
		elsif(device.device_params['VERSION'].to_i != $upload_version)
			device.reset
		end
	when :message
		if(data[:data] =~ /^CMD\/OTA: ACK/)
			if(dev_data[:state] == :UPLOADING)
				dev_data[:offset] += 120
				chunk = $upload_file.slice(dev_data[:offset], 120)

				if(chunk.nil?)
					device.send("FLASH_DONE\nLAUNCH_APP");
				else
					chunk = chunk.map { |i| sprintf('%02x', i) }.join('');
					device.send("[OTA]FLASH_WRITE #{dev_data[:offset]+FLASH_START_LOCATION} #{chunk}");
					sleep 0.02
				end
			end
		end
	end
end

devices.each do |d|
	d.on_event { |data| handle_bootloader(d, data); }
end

sleep 3

loop do
	sleep 1
	break if devices.all? { |d| !d.connected? or (d.device_params['VERSION'].to_i == $upload_version) }
end
