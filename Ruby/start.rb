
require 'active_record'
require 'yaml'

DEPLOYMENT = ENV['RAILS_ENV'] || 'production'
ARCDETECT_SERVER_PORT = ENV['ARCDETECT_SERVER_PORT']&.to_i || 9947
AR_SCRAPE_CFG = YAML.load_file('archivedb_config.yml')

# REPLACE WITH JOURNALD-LOGGER!
require 'io/console'
$stdout.sync = true

config_file = File.read('db/config.yml')
config = YAML.load(config_file)[DEPLOYMENT]

ActiveRecord::Base.establish_connection(config);

require_relative 'lib/device_con/ar_device/ar_handler.rb'
require_relative 'lib/device_con/device_server.rb'

require_relative 'lib/archive_db/archive_ar_adapter.rb'

$device_handler = DeviceCon::ARDeviceHandler.new
$server = DeviceCon::DeviceServer.new($device_handler, ARCDETECT_SERVER_PORT)

$server.start

$scrape_channels = []
$archivedb_connection = ArchiveDB::Connection.new

begin
    AR_SCRAPE_CFG['scrape_paths']&.each do |v|
        gyro = v['gyrotron']
        channel = v['channel']
        path = v['path']
        interval = v['interval']
        
        next if gyro.nil?
        next if channel.nil?
        next if path.nil?
        next if interval.nil?

        puts "Creating new AR Adapter for gyrotron #{gyro}/#{channel}!"

        $scrape_channels << ArchiveDB::ARAdapter.new($device_handler.data_queue,
            path, $archivedb_connection, 
            gyro, channel, interval)
    end
rescue
end

Thread.new do
    loop do
        sleep 1
        $scrape_channels.each(&:tick)
    end
end.abort_on_exception = true

puts "Server loaded, running on port #{ARCDETECT_SERVER_PORT}!"

$server.join 