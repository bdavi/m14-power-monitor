class AddAdcAverages < ActiveRecord::Migration[6.1]
  def change
    create_table :measurement_channels do |t|
      t.string :metric, null: false, unique: true

      t.timestamps
    end

    hypertable_opts = {
      time_column: 'time',
      chunk_time_interval: '1 day',
      compress_segmentby: 'measurement_channel_id, device_id',
      compress_orderby: 'time',
      compression_interval: '3 days'
    }

    create_table :measurement_datapoints,
      hypertable: hypertable_opts, id: false do |t|

        t.column :time, 'timestamptz', null: false
        t.references :measurement_channel, null: false
        t.references :device

        t.column :value, 'real'
    end
  end
end
