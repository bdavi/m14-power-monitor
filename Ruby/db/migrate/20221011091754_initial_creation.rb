class InitialCreation < ActiveRecord::Migration[6.1]
  def change
    enable_extension("timescaledb") unless extensions.include? "timescaledb"

    create_table :devices do |t|
      t.string :nickname, null: false, unique: true
      t.jsonb :metadata,  default: {}

      t.timestamps
    end

    create_table :log_tags do |t|
      t.string :name, null: false, unique: true
      t.timestamps
    end
    
    create_table :log_severities do |t|
      t.string :severity_key, unique: true
      t.string :colour
      t.timestamps
    end

    log_options = {
      time_column: 'time',
      chunk_time_interval: '1 day',
      compress_segmentby: 'device_id, log_tag_id, log_severity_id',
      compress_orderby: 'time',
      compression_interval: '5 days'
    }

    create_table :logs, hypertable: log_options, id: false do |t|
      t.column :time, 'timestamptz', null: false

      t.references :device
      t.references :log_tag
      t.references :log_severity

      t.string :log_line

      t.index :time
    end
  end
end
