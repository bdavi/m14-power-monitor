class AddAdcEvents < ActiveRecord::Migration[6.1]
  def change

    log_options = {
      time_column: 'time',
      chunk_time_interval: '1 day'
    }

    create_table :adc_events, hypertable: log_options, id: false do |t|
      t.column :time, 'timestamptz', null: false
      t.serial :id

      t.references :device
      t.jsonb :metadata
    end

    datapoint_options = {
      time_column: 'time',
      chunk_time_interval: '3h',
      compress_segmentby: 'adc_event_id',
      compress_orderby: 'time, datapoint_offset',
      compression_interval: '1 day'
    }

    create_table :adc_event_datapoints, 
      hypertable: datapoint_options, id: false do |t|

      t.column :time, 'timestamptz', null: false
      t.references :adc_event

      t.column :datapoint_offset, 'smallint', null: false
      t.column :adc_is, 'smallint', null: false
      t.column :adc_expected, 'smallint'
    end
  end
end
