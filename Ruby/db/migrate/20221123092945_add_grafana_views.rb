class AddGrafanaViews < ActiveRecord::Migration[6.1]
  def up
    execute <<~SQL
CREATE VIEW v_device_measurements_raw AS SELECT time, coalesce(devices.metadata->>'location', devices.nickname) AS device, metric, value
FROM measurement_datapoints 
INNER JOIN devices ON(devices.id = device_id)
INNER JOIN measurement_channels ON(measurement_channels.id = measurement_channel_id);
SQL

    execute <<~SQL
CREATE VIEW v_gyrotron_measurements_raw AS SELECT time, gyrotrons.name AS device, gyrotron_channels.channel_name AS metric, ch_max, ch_min
FROM gyrotron_measurements
INNER JOIN gyrotrons ON(gyrotrons.id = gyrotron_id)
INNER JOIN gyrotron_channels ON (gyrotron_channels.id = gyrotron_channel_id);
SQL

    execute <<~SQL
CREATE MATERIALIZED VIEW agg_device_measurements_30s WITH (timescaledb.continuous) AS
SELECT time_bucket(INTERVAL '30s', time) AS bucket, measurement_channel_id, device_id, avg(value) AS v_avg, max(value) AS v_max, min(value) AS v_min
FROM measurement_datapoints
GROUP BY bucket, measurement_channel_id, device_id
WITH NO DATA;

SELECT add_continuous_aggregate_policy('agg_device_measurements_30s', start_offset => INTERVAL '1 DAY', end_offset => INTERVAL '1 minute', schedule_interval => '1 minute');

ALTER MATERIALIZED VIEW agg_device_measurements_30s SET ( timescaledb.compress = true );
SELECT add_compression_policy('agg_device_measurements_30s', compress_after => '2 days'::interval);

CREATE VIEW v_device_measurements_30s AS SELECT bucket AS time, coalesce(devices.metadata->>'location', devices.nickname) AS device, metric, v_avg, v_max, v_min
FROM agg_device_measurements_30s
INNER JOIN devices ON (devices.id = device_id)
INNER JOIN measurement_channels ON (measurement_channels.id = measurement_channel_id);
SQL

    execute <<~SQL
CREATE MATERIALIZED VIEW agg_gyrotron_measurements_5s WITH (timescaledb.continuous) AS
SELECT time_bucket(INTERVAL '5s', time) AS bucket, gyrotron_channel_id, gyrotron_id, avg(ch_max+ch_min)/2 AS ch_avg, max(ch_max) AS ch_max, min(ch_min) AS ch_min
FROM gyrotron_measurements
GROUP BY bucket, gyrotron_channel_id, gyrotron_id
WITH NO DATA;

SELECT add_continuous_aggregate_policy('agg_gyrotron_measurements_5s', start_offset => INTERVAL '3 hours', end_offset => INTERVAL '1 minute', schedule_interval => '1 minute');

ALTER MATERIALIZED VIEW agg_gyrotron_measurements_5s SET ( timescaledb.compress = true );
SELECT add_compression_policy('agg_gyrotron_measurements_5s', compress_after => '2 days'::interval);

CREATE VIEW v_gyrotron_measurements_5s AS SELECT bucket AS time, gyrotrons.name AS device, gyrotron_channels.channel_name AS metric, ch_max, ch_min, ch_avg
FROM agg_gyrotron_measurements_5s
INNER JOIN gyrotrons ON(gyrotrons.id = gyrotron_id)
INNER JOIN gyrotron_channels ON (gyrotron_channels.id = gyrotron_channel_id);
SQL

    execute "COMMIT"

    execute "CALL refresh_continuous_aggregate('agg_device_measurements_30s', NULL, NOW()-'1 minute'::interval)"
    execute "CALL refresh_continuous_aggregate('agg_gyrotron_measurements_5s', NULL, NOW()-'1 minute'::interval)"

    execute "START TRANSACTION"
  end

  def down
    execute "DROP VIEW v_device_measurements_raw;"
    execute "DROP VIEW v_device_measurements_30s;"
    execute "DROP MATERIALIZED VIEW agg_device_measurements_30s;"

    execute "DROP VIEW v_gyrotron_measurements_raw;"
    execute "DROP VIEW v_gyrotron_measurements_5s"
    execute "DROP MATERIALIZED VIEW agg_gyrotron_measurements_5s;"
  end
end
