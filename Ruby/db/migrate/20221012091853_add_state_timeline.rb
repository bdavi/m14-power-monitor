class AddStateTimeline < ActiveRecord::Migration[6.1]
  def change
    log_options = {
      time_column: 'time',
      chunk_time_interval: '5 day',
      compress_segmentby: 'device_id',
      compress_orderby: 'time',
      compression_interval: '30 days'
    }

    create_table :device_states, id: false, hypertable: log_options do |t|
      t.string :state, null: false
      t.column :time, 'timestamptz', null: false

      t.references :device
    end
  end
end
