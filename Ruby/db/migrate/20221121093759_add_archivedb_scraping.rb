class AddArchivedbScraping < ActiveRecord::Migration[6.1]
  def change
    create_table :gyrotrons do |t|
      t.string :name, null: false, unique: true

      t.timestamps
    end

    create_table :gyrotron_channels do |t|
      t.string :channel_name, null: false, unique: true

      t.timestamps
    end

    hyp_opts = {
      time_column: 'time',
      chunk_time_interval: '1 day',
      compress_segmentby: 'gyrotron_id, gyrotron_channel_id',
      compress_orderby: 'time',
      compression_interval: '3 days'
    }

    create_table :gyrotron_measurements,
      hypertable: hyp_opts, id: false do |t|

        t.column :time, 'timestamptz', null: false

        t.references :gyrotron, null: false
        t.references :gyrotron_channel, null: false

        t.column :ch_max, 'real'
        t.column :ch_min, 'real'
    end
  end
end
