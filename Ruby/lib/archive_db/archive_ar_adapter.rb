
require_relative 'ar_defs.rb'
require_relative 'archive_signal_reader.rb'

module ArchiveDB
    class ARAdapter
        attr_accessor :exec_queue

        def initialize(queue, path, connection, gyrotron, channel, interval)
            @exec_queue = queue
            @path = path
            @connection = connection

            @interval = interval

            @ar_gyrotron = AR::Gyrotron.find_or_create_by name: gyrotron
            @ar_channel  = AR::GyrotronChannel.find_or_create_by channel_name: channel

            @last_read_attempt = AR::GyrotronMeasurement.where(
                gyrotron: @ar_gyrotron, gyrotron_channel: @ar_channel
                ).where("time > NOW() - INTERVAL '1day' ").maximum(:time)
            
            @last_read_attempt ||= Time.now() - 10*60

            @sig_reader = SignalReader.new(path, connection, interval, last_known: @last_read_attempt)
        end

        def _process_event(payload)
            case payload[:action]
            when :save_data
                ts_raw = payload[:data].times
                vs_raw = payload[:data].values
                
                ts = Array.new(ts_raw.size/2)
                v_min = Array.new(ts.size) 
                v_max = Array.new(ts.size)

                ts.each_index do |i|
                    ts[i] = ts_raw[i*2]
                    v_min[i] = vs_raw[i*2]
                    v_max[i] = vs_raw[i*2+1]
                end

                statement = <<-SQL
INSERT INTO gyrotron_measurements(time, gyrotron_id, gyrotron_channel_id, ch_min, ch_max)
VALUES (TO_TIMESTAMP(unnest($1::numeric[])), $2::int, $3::int, unnest($4::numeric[]), unnest($5::numeric[]))
SQL

                connection = AR::GyrotronMeasurement.connection.raw_connection

                connection.exec_params(statement, 
                    ["{#{ts.join(',')}}", @ar_gyrotron.id, @ar_channel.id, 
                        "{#{v_min.join(',')}}", "{#{v_max.join(',')}}"])
            end
        end

        def tick
            return if @exec_queue.nil?
            return if((Time.now().to_f - @last_read_attempt.to_f) < 20)

            @last_read_attempt = Time.now

            data = @sig_reader.next_chunk

            return if(data.times.empty?)

            @exec_queue << {
                device: self,
                action: :save_data,
                data: data
            }
        end
    end
end