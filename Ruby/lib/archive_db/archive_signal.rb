

require_relative 'archive_connection.rb'

module ArchiveDB
    class RawSignal
        attr_reader :ts_from, :ts_to
        attr_reader :last_accessed
        attr_reader :fetched_at

        def initialize(path, connection, from: nil, to: nil, nSamples: nil, interval: nil)
            @connection = connection
            @path = path

            @last_accessed = Time.at(0)

            @fetched = false
            @fetched_at = nil
            
            if(from.nil? || to.nil?)
                raise ArgumentError, "No range given!"
            end
            @ts_from = from
            @ts_to   = to
            
            @nSamples = nSamples || 
            if(@nSamples.nil?() && interval.is_a?(Numeric) && (interval > 0))
                @nSamples = ((@ts_to.to_f - @ts_from.to_f) / interval).round.to_i
            end
            
            @values = Array.new()
            @times  = Array.new()

            @unit = nil
            @datatype = nil
            @description = nil

            @pending_fetch = false
        end
        
        def fetch_again
            @fetched = false
            make_fetched
        end

        def make_fetched
            @last_accessed = Time.now();
            
            return if @fetched
            @fetched_at = Time.now()
                        
            resp = if(@nSamples)
                @connection.get(File.join(@path, '_signal.json'), from: (@ts_from.to_f * 1e9).to_i, upto: (@ts_to.to_f * 1e9).to_i, 
                reduction: 'minmax', nSamples: @nSamples);
            else
                @connection.get(File.join(@path, '_signal.json'), from: (@ts_from.to_f * 1e9).to_i, upto: (@ts_to.to_f * 1e9).to_i);
            end
            
            @pending_fetch = false
            @fetched = true
        
            if(resp == '' || resp.nil? || resp.empty?)
                resp = { 'values' => [], 'dimensions' => [] }
            end

            @values = resp['values'] || []
            @times = (resp['dimensions'] || []).map { |t| t.to_f/1e9 }

            @unit = resp['unit']
            @datatype = resp['datatype']
            @description = resp['description']

        rescue 
            puts "Error in data processing, response is: #{resp}"
            
            @fetched_at = Time.now()
            @fetched = true

            @values = []
            @times = []
        end

        def unit
            make_fetched unless @pending_fetch
            @unit
        end

        def datatype
            make_fetched unless @pending_fetch
            @datatype
        end

        def values
            make_fetched unless @pending_fetch
            @values
        end

        def times
            make_fetched unless @pending_fetch
            @times
        end

        def description
            make_fetched unless @pending_fetch
            @description
        end

        def each
            self.times().each_index do |i|
                yield(@times[i], @values[i])
            end
        end

        def inspect
            "<#W7XSignal path=#{@path} from=#{@time_range.time_from} to=#{@time_range.time_to} nSamples=#{@nSamples}>"
        end
    end
end