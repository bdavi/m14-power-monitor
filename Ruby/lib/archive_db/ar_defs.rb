
require 'active_record'
require 'timescaledb'

module AR
    class Gyrotron < ActiveRecord::Base
        has_many :gyrotron_measurements
    end

    class GyrotronChannel < ActiveRecord::Base
        has_many :gyrotron_measurements
    end

    class GyrotronMeasurement < ActiveRecord::Base
        belongs_to :gyrotron
        belongs_to :gyrotron_channel

        acts_as_hypertable
    end
end