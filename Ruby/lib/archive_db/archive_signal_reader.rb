
require_relative 'archive_signal.rb'

module ArchiveDB
    class SignalReader
        def initialize(path, connection, interval, last_known: nil)
            @path = path
            @connection = connection
            @interval = interval

            @last_known = last_known || Time.now() - 10*60
        end

        def next_chunk
            sig = @connection.signal_for(@path, from: [@last_known, Time.now() - 10*60].compact.max, to: Time.now(), interval: @interval)            
            @last_known = sig.times.max || @last_known

            return sig
        end
    end
end