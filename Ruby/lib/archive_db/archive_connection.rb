
require 'faraday'
require_relative 'archive_signal'

module ArchiveDB
    class Connection
        def initialize(url: nil, faraday: nil)
            @url = url || 'http://archive-webapi.ipp-hgw.mpg.de'

            @faraday = faraday || Faraday.new(@url,
            {
                headers: {
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                }
            }) do |f|
                f.response :json
            end
        end

        def get(path, **params)
            response = @faraday.get(path, params)

            return response.body
        end

        def signal_for(path, **params)
            return RawSignal.new(path, self, **params)
        end
    end
end