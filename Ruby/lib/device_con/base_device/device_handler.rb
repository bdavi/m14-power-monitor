
require_relative 'device.rb'

module DeviceCon
    class DeviceHandler
        attr_reader :data_queue

        attr_accessor :firmware_folder
        attr_reader   :firmware_info

        def initialize()
            @devices = Hash.new

            @data_queue = Queue.new

            @connection_lines_thread = Thread.new do
                loop do
                    payload = @data_queue.pop
                    
                    case payload
                    when :tick
                        recheck_firmwares

                        @devices.values.each &:tick
                    else
                        payload[:device]&._process_event(payload)
                    end
                end
            end

            @connection_lines_thread.abort_on_exception = true

            @tick_thread = Thread.new do
                next_tick = Time.now()
                loop do
                    @data_queue << :tick

                    next_tick += 1 until next_tick > Time.now()

                    sleep_time = (next_tick) - Time.now()
                    sleep sleep_time if sleep_time > 0
                end
            end

            @connection_lines_thread.abort_on_exception = true

            @firmware_folder = 'firmware'
            @firmware_info = Hash.new
        end

        def log(line, **data)
            device = data[:device]

            if(!device.nil?)
                puts ("%10s: " % [device.name]) + line
            else
                puts line
            end
        end

        def recheck_firmwares
            files_available = Dir.glob("*.bin", base: @firmware_folder)
            files_available.map! { |f| File.join(@firmware_folder, f) }

            f_info = {}
            files_available.each do |f|
                branch_name = File.basename(f)

                available_version = File.mtime(f).to_i

                if(@firmware_info[branch_name]&.dig(:version) == available_version)
                    f_info[branch_name] = @firmware_info[branch_name]
                    next
                end

                file_data = File.read(f).bytes

                f_info[branch_name] = {
                    filename: f,
                    version: available_version,
                    size:  file_data.size,
                    data: file_data.map { |b| sprintf('%02x', b); }.join('')
                }
            end

            @firmware_info = f_info
        end

        def _create_new(device_name)
            @devices[device_name] ||= Device.new(device_name, self)
        end

        def find_or_create(device_name)
            _create_new device_name unless @devices.include? device_name

            @devices[device_name]
        end

        def _on_event(data)

        end
    end
end