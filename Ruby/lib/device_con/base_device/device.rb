
module DeviceCon
    class Device
        attr_reader :name
        attr_reader :status_parameters
        attr_reader :state

        attr_reader :connected

        OTA_CHUNK_SIZE = 256
        FIRMWARE_START_LOCATION = 0x08100200

        def initialize(name, handler)
            @name = name
            @device_handler = handler;

            @status_parameters = Hash.new()
            @state ||= nil

            @last_message_received = nil

            set_state('DISCONNECTED')
            @connected = false

            @socket = nil

            @data_buffer = {}

            @time_synch_mutex = Mutex.new
            @last_sync_time = Time.now
            @time_at_tick_zeros = Array.new

            @command_data = {}
        end

        def log(line, **data)
            @device_handler.log(line, device: self, **data)
        end

        def firmware_branch_names
            [@name, 'main']
        end

        def available_firmware_version
            self.firmware_branch_names.each do |branch|
                fw_info = @device_handler.firmware_info[branch + '.bin']

                return fw_info unless fw_info.nil?
            end

            nil
        end

        def ota_upgrade_tick() 
            return unless @connected

            next_fw = available_firmware_version()

            return if next_fw.nil?
            return if @status_parameters['MODE'] == 'BOOTLOADER'
            return if @status_parameters['VERSION'].to_i == next_fw[:version]

            log "New firmware available (IS: #{@status_parameters['VERSION']}, AVAIL: #{next_fw[:version]}), resetting for download...", severity: :info
            reset

            _disconnect_in_thread
        end

        def ota_cmd_handle(cmd)
            if(cmd[:full_line] != "CMD/OTA: ACK\n")
                log "Problem in OTA upload, resetting device... (line was #{cmd[:full_line]})", severity: :warn

                reset
            else
                ota_chunk = @ota_firmware_binary.slice(@ota_offset*2, OTA_CHUNK_SIZE*2);

                if(ota_chunk.nil?)
                    log "OTA firmware upload complete!", severity: :info

                    send_command 'FLASH_DONE'
                    send_command 'LAUNCH_APP'
                else
                    send_command "FLASH_WRITE #{@ota_offset+FIRMWARE_START_LOCATION} #{ota_chunk}", tag: 'OTA'
                    @ota_offset += OTA_CHUNK_SIZE

                    @next_ota_tick ||= Time.now()
                    sleep_time = @next_ota_tick - Time.now()

                    sleep sleep_time if sleep_time > 0
                    @next_ota_tick += 0.02
                end
            end
        end

        def ota_connect_hook()
            selected_firmware = self.available_firmware_version
            if(!selected_firmware.nil? and @status_parameters['VERSION'].to_i != selected_firmware[:version])
                if(@status_parameters['MODE'] == 'BOOTLOADER')
                    log "Device in bootloader, starting OTA procedure!", severity: :info
                    
                    @ota_firmware_binary = selected_firmware[:data]
                    @ota_offset = 0
                    
                    send_command "FLASH_START #{FIRMWARE_START_LOCATION} #{selected_firmware[:size]} #{selected_firmware[:version]}", tag: 'OTA'
                    @next_ota_tick = Time.now() + 0.02

                    return true
                else
                    log "Firmware version is #{@status_parameters['VERSION']}, available is #{selected_firmware[:version]}!", severity: :info

                    reset
                    _disconnect_in_thread
                    return true
                end
            elsif(@status_parameters['MODE'] == 'BOOTLOADER' and @status_parameters['VERSION'].to_i != -1)
                send_command 'LAUNCH_APP'
                return true
            end

            return false
        end

        def tick
            ota_upgrade_tick();

            if((@time_at_tick_zeros.size < 45) || (Time.now - @last_sync_time > 9.5))
                _send_sync_start
            end

            if(@connected && (Time.now - @last_message_received > 10))
                log "Disconnecting due to timeout! (no message received for >10 seconds)", severity: :warn
                _disconnect_in_thread
            end
        end

        def _io_loop(socket)
            log "Entering loop with socket #{socket}"
            loop do
                l = socket.gets

                @last_message_received = Time.now()

                case l
                when /^CMD\/TSYNC/
                    _process_tsych_line(l)
                else
                    @device_handler.data_queue << {
                        time: Time.now(),
                        device: self,
                        line: l,
                        type: :line
                    }
                end
            end
        ensure
            log("Socket #{socket} disconnected from io loop!");
            _disconnect
        end

        def _process_log_message(msg)
            # puts ("%10s: " % [@name]) + msg[:full_line]
        end
        def _process_databatch(data)
        end

        def tick_to_time(ts)
            @time_synch_mutex.synchronize do
                return Time.now() if @time_at_tick_zero.nil?


                ret_time = @time_at_tick_zero + (ts.to_f / 1000.0)
                # Something's wrong if a message is this far delayed
                if((Time.now() - ret_time) > 60)
                    @time_at_tick_zero = nil
                    @time_at_tick_zeros = []

                    return Time.now()
                end

                return ret_time
            end
        end

        def _process_tsych_line(line)
            match = /CMD\/TSYNC:\s(?<tick>\d+)\s(?<ts>[\d\.]+)/.match(line)
            return if match.nil?

            time_sent = Time.at(match['ts'].to_f)
            tick_recv = match['tick'].to_i

            rtt = Time.now() - time_sent

            time_recv = time_sent + 0.5*rtt

            time_at_tick_zero = time_recv - tick_recv/1000.0

            @time_synch_mutex.synchronize do
                # We need to force a reset when the new zeroing time diverges majorly from the 
                # current assumption. This happens at e.g. a device reset
                if(@time_at_tick_zero && ((time_at_tick_zero - @time_at_tick_zero).abs > 10))
                    @time_at_tick_zero = nil
                    @time_at_tick_zeros = []
                end

                @time_at_tick_zeros << time_at_tick_zero.to_f
                if(@time_at_tick_zeros.size > 60)
                    @time_at_tick_zeros.shift
                end

                samp_start = 0.2*@time_at_tick_zeros.size.floor
                samp_end   = 0.8*@time_at_tick_zeros.size.ceil

                samp_arr = @time_at_tick_zeros.sort[samp_start..samp_end];

                @time_at_tick_zero = Time.at(samp_arr.sum.to_f / samp_arr.size)
            end 

            @device_handler.data_queue << {
                time: Time.now(),
                device: self,
                type: :tsync,
                rtt: rtt,
                tatz: @time_at_tick_zero
            }
        end

        def _process_cmd(cmd)
            case cmd[:tag]
            when 'OTA'
                ota_cmd_handle(cmd)
            end
        end

        def _send_sync_start
            @last_sync_time = Time.now
            send_command "TSYNC! #{Time.now().to_f.round(4)}", tag: "TSYNC"
        end

        def state=(new_state)
            return if new_state == @state

            log "New state is #{new_state}", severity: :debug
            @state = new_state
        end
        def set_state(new_state)
            self.state = new_state
        end
        
        def _process_event(data)
            case data[:type]
            when :line
                _process_line(data)
            when :disconnect
                _disconnect_in_thread
            end
        end

        def _process_line(data)
            line = data[:line]

            # Filter out colourations
            line.gsub!(/\x1b\[[^m]+m/, '');

            case line
            when /^(?<sev>\S+)\s*\[(?<ts>[^\]]+)\]\s*(?<tag>[^:]+):\s*(?<msg>.+)$/

                msg = {
                    full_line: line,
                    sev: $~[:sev],
                    tag: $~[:tag],
                    device: self,
                    msg: $~[:msg]
                }

                _process_log_message(msg)
            when /^CMD(?:\/(?<tag>[^:]+)):\s+(?<reply>.+)/
                _process_cmd({
                    recv_time: data[:time],
                    full_line: line,
                    tag: $~[:tag],
                    device: self,
                    reply: $~[:reply]
                })
            when /^STATE: (.*)$/
                @state_last_received = Time.now();
                set_state($1.split(',').join(' '));
            when /^(?<tag>[^:]+):\s*START\s*(?<data>.*)$/
                # puts ("%10s: Starting data batch #{$~[:tag]}" % [@name])
                @data_buffer[$~[:tag]] = {
                    tag: $~[:tag],
                    tstart: data[:time],
                    start_data: $~[:data],
                    data: Array.new,
                    device: self
                };
            when /^(?<tag>[^:]+):\s*(DONE|END)/
                return if @data_buffer[$~[:tag]].nil?

                _process_databatch(@data_buffer[$~[:tag]]);
                # puts ("%10s: Completed data batch #{$~[:tag]}" % [@name])
            when /^(?<tag>[^:]+):(?<data>.*)$/
                if @data_buffer[$~[:tag]].nil?
                    _process_databatch({
                        tag: $~[:tag],
                        tstart: data[:time],
                        data: [$~[:data]],
                        device: self
                    })
                else
                @data_buffer[$~[:tag]][:data] << $~[:data]
                end
            else
                log("Unknown LogLink line received: " + line, severity: :warn)
            end

            match = /^(?<tag>[^:]+):(?:\s+)/
        end

        def _connect(line, socket)
            log("Starting new connection with socket #{socket}...")
            _disconnect_in_thread

            @socket = socket

            if(line =~ /^CONNECT (.+)$/)
                param_list = $1.split(" ");
                param_list.each do |p|
                    if(p =~ /(.*)=(.*)/)
                        @status_parameters[$1] = $2 
                    end
                end

                @last_message_received = Time.now

                return if ota_connect_hook

                if(@status_parameters.include? 'STATE')
                    set_state @status_parameters['STATE']
                elsif(@status_parameters['MODE'] == 'BOOTLOADER')
                    set_state('BOOTLOADER')
                else
                    set_state('CONNECTED')
                end

                @connected = true
            end
        end

        def _disconnect_in_thread
            set_state('DISCONNECTED')

            @connected = false

            begin
                @socket&.close
                log("Closed socket #{@socket}!")
            rescue
                # Dumb rescue because we really don't care about that close
            end
        end

        def _disconnect
            return unless @connected

            @connected = false

            @device_handler.data_queue << {
                device: self,
                type: :disconnect
            }
        end

        def connected?
            return @connected
        end

        def send_command(cmd, tag: nil)
            return if @socket.nil?
            return if @socket.closed?

            begin
                @socket.puts (((tag.nil?) ? '' : "[#{tag}]") + cmd) + "\n"
            rescue IOError
                _disconnect

                return false
            end

            true
        end

        def reset
            send_command('RESET')
            @time_at_tick_zero = Time.now
            @time_at_tick_zeros = []
        end

        def inspect
            "#<ArcDetectDevice name=#{@name} state=#{@state}>"
        end
    end
end