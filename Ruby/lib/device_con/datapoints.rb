
require 'base64'

module DeviceCon
    class DatapointSet
        attr_reader :valid
        attr_reader :channel_count

        attr_reader :timestamps
        attr_reader :channels

        attr_reader :tstart
        attr_reader :ref_timestamp

        def initialize(data_packet)
            @valid = false
            @tstart = data_packet[:tstart]

            datapoint_match = /DATAPOINTS (?<ts>\d+)l?\s+(?<count>\d+)\s+(?<channels>\d+)/.match(data_packet[:start_data])

            if datapoint_match.nil?
                puts "No valid match found in line #{data_packet[:start_data]}!"
                return
            end

            @channel_count = datapoint_match[:channels].to_i
            @ref_timestamp = datapoint_match[:ts].to_i

            line_count = 0;

            timestamps = ''

            d_bunches = data_packet[:data].join('').gsub(/[\s\n]/, '').split('---', -1)

            if(d_bunches.any? nil)
                puts "Databatch contained nil value, returning!"
            end

            timestamps = Base64.decode64(d_bunches[0]).unpack('S*')

            @channels = []
            channel_count.times do |c|
                @channels << Base64.decode64(d_bunches[c+1]).unpack('s*')
            end

            @channels.each do |c|
                if(c.size != timestamps.size)
                    puts "Uneven data batch size, possibly corrupted?"
                    return
                end
            end

            @valid = true

            chomp_timestamp = @ref_timestamp % 0x10000
            @timestamps = timestamps.map do |t|
                tdiff = t - chomp_timestamp
                tdiff -= 0x10000 if tdiff > 0

                tdiff + @ref_timestamp
            end

        rescue
            puts "Unknown error in data processing!"
            
            @valid = false
        end
    end
end