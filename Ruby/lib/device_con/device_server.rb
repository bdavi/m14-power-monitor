
require 'socket'
require 'timeout'

module DeviceCon
    class DeviceSocket
        def initialize(server, socket)
            @server = server
            @socket = socket

            @process_queue = server.device_handler.data_queue

            @device = nil

            @last_ping_at = Time.now();

            @read_thread = Thread.new do 
                _handle_socket

            rescue Timeout::Error, Errno::ECONNRESET, IOError
            ensure
                @device._disconnect unless @device.nil?
                @server.open_sockets.delete self
                begin
                    @socket.close
                rescue
                end
            end

            @read_thread.abort_on_exception = true
        end

        def _process_event(data)
            case data[:type]
            when :connect
                device = @server._grab_device(data[:params]['NAME'])
                device._connect(data[:line], @socket)

                @device = device

                @read_thread.run
            end
        end

        def _handle_socket
            l = ''

            Timeout.timeout(30) do
                loop do
                    l = @socket.gets()

                    match = /^CONNECT (.+)$/.match(l);
                    next if match.nil?
                    
                    param_hash = Hash.new

                    param_list = match[1].split(" ");
                    param_list.each do |p|
                        if(p =~ /(.*)=(.*)/)
                            param_hash[$1] = $2 
                        end
                    end
                    
                    if(param_hash.include? 'NAME')
                        @process_queue << {
                            device: self,
                            type: :connect,
                            line: l,
                            params: param_hash
                        };

                        Thread.stop() while @device.nil?
                        
                        break;
                    end
                end
            end
            
            @device&._io_loop(@socket)
        end
    end

    class DeviceServer
        attr_reader :device_handler
        attr_reader :open_sockets

        def initialize(handler, port = 9947)
            @port = port
            @loop_thread = nil

            @device_handler = handler
            @open_sockets = Array.new
        end

        def start
            @loop_thread = Thread.new do
                Socket.tcp_server_loop(@port) do |socket|
                    _handle_socket(socket);
                end
            end

            @loop_thread.abort_on_exception = true

            @loop_thread
        end

        def _handle_socket(socket)
            @open_sockets << DeviceSocket.new(self, socket);
        end

        def _grab_device(name)
            @device_handler.find_or_create name
        end

        def join
            @loop_thread.join
        end
    end
end