
require_relative 'ar_defs.rb'
require_relative '../base_device/device.rb'
require_relative '../datapoints.rb'

require 'base64'

module DeviceCon
    class ARDevice < Device
        LOCATION_MAPPING = {
            'A' => 'Alpha', 'B' => 'Beta', 'C' => 'Charly', 'D' => 'Delta', 'E' => 'Echo', 'F' => 'Foxtrot'
        }

        def initialize(name, handler)
            @db_device = AR::Device.find_or_create_by nickname: name
            
            @state = @db_device.metadata['_state']
            @state_last_logged = Time.now();

            if(@db_device.metadata['location'].nil?)
                location = name;
                case name
                when /^(\w+)[\s\-_]*(\d+)/
                    location = "#{$1.capitalize} #{$2}"
                when /^([a-fA-F])(\d+)/
                    location = "#{LOCATION_MAPPING[$1.upcase]} #{$2}"
                end

                @db_device.metadata['location'] = location
                @db_device.save
            end

            super(name, handler)
        end

        def firmware_branch_names
            @db_device.metadata['firmware_branches'] || [@name, 'main']
        end

        def _process_event(data)
            case data[:type]
            when :tsync
                @tsync_rtt_channel ||= AR::MeasurementChannel.find_or_create_by metric: :rtt

                AR::MeasurementDatapoint.create(device: @db_device, 
                    measurement_channel: @tsync_rtt_channel, 
                    time: data[:time], value: data[:rtt])
            end

            super(data)
        end

        def _process_log_message(msg)
            super(msg)

            sev = AR::LogSeverity.find_or_create_by severity_key: msg[:sev]
            tag = AR::LogTag.find_or_create_by name: msg[:tag]

            @db_device.logs.create(log_tag: tag, log_severity: sev, 
                log_line: msg[:msg], time: Time.now())
        end

        def _process_databatch(data)
            case data[:tag]
            when "ADC/ARC_TIMING"
                match = /^\s(\d+)\s(\d+)/.match(data[:data][0])
                return if match.nil?

                start_time = tick_to_time(match[1].to_i)
                duration = (match[2].to_i - match[1].to_i).to_f / 1000
                
                @arc_duration_channel ||= AR::MeasurementChannel.find_or_create_by metric: 'arc_duration'

                @db_device.measurement_datapoints.create(measurement_channel: @arc_duration_channel, value: duration, time: start_time);

            when "ADC/EVT"

                data_str = data[:data].join('').gsub(/\s*/, '').gsub('\n', '');

                
                data_str = Base64.decode64(data_str);
                data_str = data_str.unpack("lls*");
                
                trig_time = data_str.shift.to_i
                trig_time = nil if trig_time == -1

                trig_tick = data_str.shift.to_i

                packet_length = 2048

                adc_is_data = data_str.slice(0, packet_length);
                adc_exp_data = data_str.slice(packet_length, packet_length);

                return if adc_is_data.nil? or adc_exp_data.nil?

                offset_min = (-adc_is_data.size/2).ceil
                offset_max = offset_min + adc_is_data.size - 1;

                evt = @db_device.adc_events.create(time: tick_to_time(trig_tick), metadata: { 'trigger_point' => trig_time });

                statement = <<-SQL
INSERT INTO adc_event_datapoints(time, adc_event_id, datapoint_offset, adc_is, adc_expected)
VALUES ($1::timestamp, $2::int, generate_series($3::int, $4::int), unnest($5::int[]), unnest($6::int[]));
SQL
                AR::AdcEventDatapoint.connection.raw_connection.exec_params(statement, [evt.time, evt.id, offset_min, offset_max, '{' + adc_is_data.join(',') + '}', '{'+adc_exp_data.join(',')+'}']);

            when "ADC/AVG"
                @adc_avg_dataset ||= nil
                
                data_set = DatapointSet.new(data)

                return unless data_set.valid
                @adc_avg_dataset = data_set

                @adc_channel ||= AR::MeasurementChannel.find_or_create_by metric: 'adc_avg'
                @target_channel ||= AR::MeasurementChannel.find_or_create_by metric: 'adc_expected'
                @m1_power_channel ||= AR::MeasurementChannel.find_or_create_by metric: 'm1_power'
                @noise_channel ||= AR::MeasurementChannel.find_or_create_by metric: 'adc_noise'

                connection = AR::MeasurementDatapoint.connection.raw_connection

                statement = <<-SQL
INSERT INTO measurement_datapoints(time, device_id, measurement_channel_id, value)
VALUES (TO_TIMESTAMP(unnest($1::numeric[])), $2::int, $3::int, unnest($4::numeric[]))
SQL

                map_ts = data_set.timestamps.map { |t| tick_to_time(t).to_f }
                
                connection.exec_params(statement, ["{#{map_ts.join(',')}}", @db_device.id, @adc_channel.id, "{#{data_set.channels[0].join(',')}}"])
                connection.exec_params(statement, ["{#{map_ts.join(',')}}", @db_device.id, @target_channel.id, "{#{data_set.channels[1].join(',')}}"])
                connection.exec_params(statement, ["{#{map_ts.join(',')}}", @db_device.id, @m1_power_channel.id, "{#{data_set.channels[2].join(',')}}"])
                connection.exec_params(statement, ["{#{map_ts.join(',')}}", @db_device.id, @noise_channel.id, "{#{data_set.channels[3].join(',')}}"])
            end
        end

        def state=(new_state)
            if((new_state != @state) || (Time.now() - @state_last_logged > 60))
                state_str = new_state&.to_s || 'ERR UNKNOWN'
                
                @db_device.device_states.create(time: Time.now(), state: state_str)
                @db_device.metadata['_state'] = state_str;
                @db_device.save
                @state_last_logged = Time.now()
            end

            super(new_state)
        end
    end
end 
