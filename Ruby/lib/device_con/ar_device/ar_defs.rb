
require 'active_record'
require 'timescaledb'

module AR
    class LogSeverity < ActiveRecord::Base
        has_many :logs
    end
    
    class LogTag < ActiveRecord::Base
        has_many :logs
    end

    class Log < ActiveRecord::Base
        belongs_to :device
        belongs_to :log_severity
        belongs_to :log_tag

        acts_as_hypertable
    end

    class Device < ActiveRecord::Base
        has_many :logs
        has_many :device_states

        has_many :adc_events
        has_many :measurement_datapoints
    end

    class DeviceState < ActiveRecord::Base
        belongs_to :device

        acts_as_hypertable
    end

    class AdcEvent < ActiveRecord::Base
        belongs_to :device
        self.primary_key = :id

        has_many :adc_event_datapoints

        acts_as_hypertable
    end
    class AdcEventDatapoint < ActiveRecord::Base
        belongs_to :adc_event

        acts_as_hypertable
    end


    class MeasurementChannel < ActiveRecord::Base
        has_many :measurement_datapoints
    end
    class MeasurementDatapoint < ActiveRecord::Base 
        belongs_to :measurement_channel
        belongs_to :device

        acts_as_hypertable
    end
end