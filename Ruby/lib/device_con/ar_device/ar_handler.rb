

require_relative 'ar_device.rb'
require_relative '../base_device/device_handler.rb'


module DeviceCon
    class ARDeviceHandler < DeviceHandler
        def initialize()
            super()

            AR::Device.all.each do |d|
                _create_new d.nickname
            end
        end

        def _create_new(device_name)
            @devices[device_name] ||= ARDevice.new(device_name, self);
        end
    end
end
