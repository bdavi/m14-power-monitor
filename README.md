## Quick links:
- [GitLab Page here](https://gitlab.mpcdf.mpg.de/bdavi/m14-power-monitor)
- [Wiki Pages here](https://gitlab.mpcdf.mpg.de/bdavi/m14-power-monitor/-/wikis/home), including a [board setup guide](https://gitlab.mpcdf.mpg.de/bdavi/m14-power-monitor/-/wikis/Getting%20Started) and [server setup guide](https://gitlab.mpcdf.mpg.de/bdavi/m14-power-monitor/-/wikis/Server%20Setup)!
- [Device TCP protocol/command description](https://gitlab.mpcdf.mpg.de/bdavi/m14-power-monitor/-/wikis/communication/loglink.md)

## Getting the code
This project uses submodules!! Make sure you run the appropriate pull command:
```
git pull --recursive
```

# M14 Power Monitor and Arc Detector
This repository contains files relevant for the M14/M1 mirror power detector. These systems are responsible for detecting the input- and output power of the gyrotrons through the open-air microwave beam path, and are meant to monitor the formation of arcing within.

These systems are not necessarily meant to provide high accuracy readouts of the momentary microwave power. They are instead optimized to provide fast and accurate fault detection by detecting a sudden increase in on-route losses, caused by the formation of an arc flash.

## Project Structure

| Folder | Purpose |
| --- | --- |
| `Discussion` and `ProgressDocumentation` | Various files related to the design process. Not needed to build or design anything. |
| `Grafana` | Includes JSON Grafana dashboard templates and SQL queries for Grafana alerts |
| `Nucleo_M14_Hat` | Altium files for the physical hardware. The board is designed as a Hat for the NUCLEO-F439ZI STM32 development board. |
| `Ruby` | Contains the Ruby server-side code. See [the server getting-started] for how to set it up, as well as the [server-code documentation](https://gitlab.mpcdf.mpg.de/bdavi/m14-power-monitor/-/wikis/Ruby-server-code) if you want to modify it. |
| `STMCubeWorkspace` | Contains the C/C++ code needed to compile the firmware for the board. [More details here](https://gitlab.mpcdf.mpg.de/bdavi/m14-power-monitor/-/wikis/STM32-Compile-Environment) |
| `mpg-stm32` and `altium-libraries` | Git Submodules, linking to STM32 Code and Altium components needed for this project |
