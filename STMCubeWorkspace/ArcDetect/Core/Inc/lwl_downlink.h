/**
 * @file lwl_downlink.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief Fiberoptics downlink setup and processing
 * @version 1.0
 * @date 2022-09-14
 * 
 * @copyright Copyright (c) 2022 Max Planck Institute 
 * 		of Plasma Physics Greifswald
 * 
 */

#ifndef INC_LWL_DOWNLINK_H_
#define INC_LWL_DOWNLINK_H_

#include <stddef.h>

namespace SYS {
namespace LWL {

enum downlink_keys_t : uint8_t {
	DOWN_ARC_DETECTED_0 = 0,
	DOWN_LOG_LOW = 4,
	DOWN_LOG_HIGH = 5,
	DOWN_KEEPALIVE = 8,
	DOWN_ARC_DETECTED_1 = 170
};

enum uplink_keys_t : uint8_t {
	UP_POWER_LOW = 4,
	UP_POWER_HIGH = 5,
	UP_LOOPBACK = 8,
	UP_KEEPALIVE = 9
};

#pragma pack(1)
struct downlink_ping_data_t {
	uint8_t key:4;
	uint8_t conn_ok:1;
	uint8_t hw_ok:1;
	uint8_t armed:1;
	uint8_t _one:1;
};
#pragma pack(0)

void mark_arc();

bool has_link();
bool is_loopbacked();

extern volatile int32_t current_power;
extern volatile int gyrotron_warmup_ticks;
extern volatile int key_data_error;
extern volatile uint32_t key_data_error_info;

}
}
#endif /* INC_LWL_DOWNLINK_H_ */
