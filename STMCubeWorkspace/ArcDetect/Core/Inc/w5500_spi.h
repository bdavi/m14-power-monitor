/**
 * @file w5500_spi.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief Ethernet SPI setup code
 * @version 1.0
 * @date 2022-09-02
 * 
 * @copyright Copyright (c) 2022 Max Planck Institute 
 * 		of Plasma Physics Greifswald
 * 
 */

#ifndef INC_W5500_SPI_H_
#define INC_W5500_SPI_H_

#include <all/spi/spi.h>

namespace SYS {

extern SPI::Master spi5;

void w5500_spi_init();

}

#endif /* INC_W5500_SPI_H_ */
