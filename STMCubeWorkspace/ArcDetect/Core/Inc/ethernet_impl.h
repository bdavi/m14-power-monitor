/**
 * @file ethernet_impl.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief Ethernet initialization and handling code
 * @version 1.0
 * @date 2022-09-02
 * 
 * @copyright Copyright (c) 2022 Max Planck Institute 
 * 		of Plasma Physics Greifswald
 * 
 */

#ifndef INC_ETHERNET_IMPL_H_
#define INC_ETHERNET_IMPL_H_

#include <all/w5500/w5500.h>

namespace SYS {
namespace Ethernet {

extern W5500::Core phy;
extern W5500::BaseSocket log_tcp_server;
extern W5500::BaseSocket log_tcp_client;

}
}

extern "C"
void ethernet_task_fn(void *args) __attribute__((__noreturn__));

#endif /* INC_ETHERNET_IMPL_H_ */
