/**
 * @file command_glue.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief Command-Glue code
 * @details This code is responsible for all command actions
 *   that user and server can perform. The init() function
 *   calls the necessary setup to add hooks and processing
 *   abilities, while internal functions check for, and 
 *   execute typed commands.
 * 
 * @version 1.0
 * @date 2022-11-07
 * 
 * @copyright Copyright (c) 2022 Max Planck Institute 
 * 		of Plasma Physics Greifswald
 * 
 */

#ifndef INC_COMMAND_GLUE_H_
#define INC_COMMAND_GLUE_H_

#include <stdint.h>
#include <all/commands/command_handler.h>

namespace SYS {
namespace CMD {

extern CommandBuffer log_server_buffer;
extern CommandBuffer log_client_buffer;

void set_ipcfg_item(CommandBuffer & cmd, uint8_t * ptr);

void init();

}
}


#endif /* INC_COMMAND_GLUE_H_ */
