/**
 * @file adc_task.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief ADC Task post-processing header file
 * @version 1.0
 * @date 2022-09-14
 * 
 * @copyright Copyright (c) 2022 Max Planck Institute 
 * 		of Plasma Physics Greifswald
 * 
 */

#ifndef INC_ADC_TASK_H_
#define INC_ADC_TASK_H_

#include <FreeRTOS.h>

namespace SYS {
namespace ADCTask {

// FreeRTOS Tick of the last known Arc event, 
// used by the Indicator thread to flash the LED
extern TickType_t last_event_tick;

// Copy of FreeRTOS ticks, accessible to the 
// high-priority ADC ISR
extern TickType_t adc_meas_tick;

// Send a Task Notification to the ADC task
void trigger_task_from_isr();

void tick_hook();

// Returns true if the last ADC Selftest returned OK.
// Used by the Indicator thread for system state indication
bool hw_ok();

}
}

extern "C" void adc_tick_hook();

#endif /* INC_ADC_TASK_H_ */
