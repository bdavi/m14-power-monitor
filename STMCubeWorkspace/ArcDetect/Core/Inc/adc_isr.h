/**
 * @file adc_isr.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief Config file for all ADC ISR capture related settings
 * @version 1.0
 * @date 2022-09-09
 * 
 * @copyright Copyright (c) 2022 Max Planck Institute 
 * 		of Plasma Physics Greifswald
 * 
 */

#ifndef INC_ADC_ISR_H_
#define INC_ADC_ISR_H_

#include <stdint.h>

#define CCM_DATA __attribute__((section (".ccmram")))


// LEAVE COMMENTED FOR PRODUCTION
// This flag will change the ADC behaviour to save the value directly received from
// the fiber system (range 0 to 255) instead of the expected power value. Useful for
// debugging
// #define ADC_ISR_SEND_CURRENT_POWER

// ADC value representing 0V at scale. Not significant, as
// adc_zero_adjust takes care of automatic fine-tuning
#define ADC_DATA_CENTER 8064

// How large must the ADC signal be before arc detection is viable?
// This must be large enough to at least ensure that noise does not 
// cause false positives
#define ADC_ARCING_MINIMUM_AMPLITUDE 200

// These values define the smoothing setup of the respective arcing
// and anomaly detection code. The math is as follows:
// smoothed_value = (smoothed_value * (1 - 1/SMOOTHING_FACTOR) + measured_value * 1/SMOOTHING_FACTOR)
// Which is equivalent to a PT1 element. USE NO MORE THAN 1<<16!! Higher values risk overflow.
// As such, smoothing is limited to about 100ms
#define ADC_ARCING_SMOOTHING_FACTOR (1<<13)
#define ADC_ANOMALY_SMOOTHING_FACTOR (1<<11)

// Power threshold of arc detection.
// If the normalized power level (((adc_data<<16)/power_detect_expected_level))
// drops below this level, an arc will be triggered immediately.
#define ADC_ARCING_THRESHOLD_LEVEL int32_t(0.1F*65536)
#define ADC_ARCING_THRESHOLD_COUNTER 3

// Number of samples for the anomaly sample buffer
#define ADC_ANOMALY_SAMPLE_BUFFER_SIZE 2048

// Count, in bits, that defines how far the signal must deviate from
// its expected value to trigger the anomaly detection
#define ADC_ANOMALY_TRIGGER_THRESHOLD 400

// How many samples must, consecutively, be above the trigger threshold
// before an anomaly is detected?
#define ADC_ANOMALY_TRIGGER_COUNT 10

// How many samples are kept in buffer before a trigger event is detected?
#define ADC_ANOMALY_PRESAMPLING_COUNT 512


/**
 * @brief ADC High-Speed sampling namespace
 * 
 * @details This namespace contains the code needed to 
 *   provide high-speed (500kHz) read-out of the ADC 
 *   and per-sample triggering.
 * 
 *   It handles the ADC and PGA SPI connections and setup,
 *   reasonably filters the data, modulates threshold values
 *   based on the SYS::LWL::current_power value, and 
 *   triggers the LWL alert as well as the ADC task upon 
 *   detecting an arc fault.
 * 
 *   Please note: Do read the documentation at
 *   https://gitlab.mpcdf.mpg.de/bdavi/m14-power-monitor/-/wikis/Internals/ADC-Systems
 * 
 *   This code was not written to read easily. It was written to reduce number of
 *   instructions executed.
 * 
 *   @note If anyone wishes to implement this on a separate FPGA, that would be great!
 * 
 */
namespace Sampler {


#pragma pack(1)

//! Internally used struct for the PGA11x series of chips 
typedef struct {
	uint8_t chsel:4;	//!< Input channel selection
	uint8_t gsel:4;		//!< Gain selection
	uint8_t cmd_type:8; //!< Read/Write/Chain command select (consult PGA docs)
} pga_11x_packet_t;

//! Internally used struct defining a sample buffer element
typedef struct {
	int16_t adc_is;		//!< Measured ADC value at this sample
	int16_t adc_exp;	//!< Expected ADC value based on the M1 Power estimation
} sample_data_t;
#pragma pack(0)

//! States of the internal sampler engine
enum sample_buffer_state_t {
	LOCKED,				//!< Sampler has finished and is waiting on the ADC task
	RESTART,			//!< Sampler will reinitialize its values and restart
	PRESAMPLING,		//!< Sampler is filling a minimum initial count of samples
	WAIT_FOR_TRIG,		//!< Sampler is waiting for a trigger event to occur
	POSTSAMPLING		//!< Sampler is recording samples post-event
};

/**
 * @brief Externally set zeroing parameter. 
 * @details This value is set by the ADC Task to correct for DC offset. It does this
 *   by filtering the adc itself over a long period of time. 
 *   Note that DC offset correction calculation is paused when the gyrotron power
 *   is off, to not adjust away the real, measured signal during runs
 */
extern volatile int32_t adc_zero_adjust;

//! ADC value, smoothed with a 2ms PT1 to provide aliasing-free 1kHz samples for the ADC Task power logging
extern volatile int32_t adc_smoothed;
extern volatile int32_t adc_noise;

//! Internal value of the Arc power detection. Used by the command_glue to fake an arc.
extern int32_t normalized_power_smoothed;
//! Internal value of the Arc power detection algorithm. Used by the ADC Task to log power
extern volatile int32_t power_detect_expected_adc;

/**
 * @brief Circular sample buffer for anomaly detections.
 * @details This circular buffer is continuously written by the sampler engine when it is
 *   in sampling mode. Note that it is written in reverse order, e.g. from 2047 down to 0,
 *   and must be unrolled. sample_buffer_end defines the next position at which the 
 *   sampler would have written. Unroll in reverse from there.
 * 
 */
extern sample_data_t sample_buffer[];
//! Defines the stop point of the currently held sample. Unroll the buffer in reverse direction from it.
extern volatile int32_t sample_buffer_end;
//! Defines the position inside the current sample buffer at which an arc was detected.
extern volatile int32_t sample_buffer_arc_pos;
//! FreeRTOS tick at which the anomaly sample was captured. Used by the ADC Task during logging 
extern volatile uint32_t sample_buffer_trigger_tick;

//! Flag for the ADC Task to indicate that a new arc was detected
extern volatile bool       arc_detect_flag;
//! FreeRTOS Tick at which the arc was first detected
extern volatile uint32_t arc_start_tick;
//! FreeRTOS Tick at which the arc was last detected
extern volatile uint32_t arc_end_tick;

//! Internal buffer containing values of the selftest routine. Used by the ADC Task Selftest function
extern volatile uint16_t  selftest_buffer[];

//! Start the ISR and run the sample monitoring system
void start_monitoring();
//! Trigger a selftest. Note that the system will automatically go back into monitoring mode after.
void trigger_selftest();
//! Clear the selftest-done flag.
void clear_selftest();
//! Returns true if the last selftest has completed and has not been read yet.
bool selftest_done();

//! Reset the sampler and re-start it
void restart_sampler();
//! Returns true if a sample is held in the buffer, e.g. sample_buffer_state = LOCKED
bool sample_available();
//! Force a measurement of the Sampler system to trigger. Used by command_glue "TRIG" command
void force_measurement();

}

#endif /* INC_ADC_ISR_H_ */
