/**
 * @file config_data.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief Flash-based config data and assorted settings
 * @details This code is responsible for saving and processing
 *   all non-volatile settings that have to be stored in Flash.
 *   Due to STM32 flash erase procedures, this requires an entire
 *   flash bank to be reserved for device configuration settings.
 * @version 1.0
 * @date 2022-09-21
 * 
 * @copyright Copyright (c) 2022 Max Planck Institute 
 * 		of Plasma Physics Greifswald
 * 
 */

#ifndef INC_CONFIG_DATA_H_
#define INC_CONFIG_DATA_H_

#include <all/w5500/defs.h>

#define DEVICE_CONFIG_LOCATION 0x0800c000
#define DEVICE_CONFIG_MAGIC_1  0x712BF58B
#define DEVICE_CONFIG_MAGIC_2  0xB85FB217

#define PROGRAM_CONFIG_LOCATION 0x08100000

struct device_config_t {
	uint32_t _magic_1;
	char name[16];
	char shared_key[16];
	char branch[16];
	W5500::REGS::crb_address_config_t own_addr_config;
	uint8_t  main_server_ip[4];
	uint16_t main_server_port;
	uint32_t _magic_2;
};

struct program_config_t {
	uint32_t _magic_1;
	long int crc32;
	long int version_nr;
	uint32_t _magic_2;
};

#define MAIN_PROGRAM_CFG (reinterpret_cast<const program_config_t*>(PROGRAM_CONFIG_LOCATION))

extern device_config_t device_cfg;

extern "C" void load_device_config();
void save_device_config();

#endif /* INC_CONFIG_DATA_H_ */
