/*
 * indicator_driver.cpp
 *
 *  Created on: Sep 2, 2022
 *      Author: xaseiresh
 */

#include "main.h"

#include "ethernet_impl.h"
#include "lwl_downlink.h"
#include "adc_task.h"

#include <all/log/log.h>
#include <all/indicator_led/led.h>

#include <FreeRTOS.h>
#include <task.h>

#include <cstring>

#define INDICATOR_SPI hspi4



namespace SYS {
namespace Indicator {

enum system_level_t {
	INACTIVE,
	ACTIVE,
	EVENT,
	WARNING,
	ERROR
};

system_level_t current_level = INACTIVE;

struct system_fault_source_t;
system_fault_source_t * head_fault_source;

struct system_fault_source_t {
	const char * item_key;
	const system_level_t level;

	bool active;
	bool was_active;

	system_fault_source_t * next_source;

	system_fault_source_t(const char * key, system_level_t level)
	: item_key(key), level(level), active(false), was_active(false) {
		next_source = head_fault_source;
		head_fault_source = this;
	};
};

typedef system_fault_source_t fsrc_t;

fsrc_t ethernet_no_phy_fault("NO_ETH_PHY", ERROR);
fsrc_t ethernet_no_link_fault("NO_ETH_LINK", WARNING);
fsrc_t ethernet_no_server_fault("NO_SERVER", WARNING);
fsrc_t ethernet_client_connected("CLIENT_CONNECTED", ACTIVE);

fsrc_t adc_hardware_fault("ADC_FAULT", ERROR);
fsrc_t adc_event_fault("ADC_EVENT", EVENT);

fsrc_t lwl_data_fault("LWL_ERROR", WARNING);
fsrc_t lwl_no_link_fault("LWL_OFFLINE", WARNING);

uint8_t led_data_buffer[13] = {};
Indicators::LED status_led;

bool any_errors() {
	if(!SYS::Ethernet::phy.is_initialied())
		return true;
	if(!SYS::ADCTask::hw_ok())
		return true;

	return false;
}
bool any_warnings() {
	if(!SYS::Ethernet::phy.has_link())
		return true;
	if((!SYS::LWL::has_link()) || (SYS::LWL::key_data_error != 0))
		return true;
	if(!SYS::Ethernet::log_tcp_client.is_connected())
		return true;

	return false;
}
bool any_active() {
	if(SYS::Ethernet::log_tcp_server.is_connected())
		return true;

	return false;
}

void check_state() {
	static TickType_t last_state_pub = xTaskGetTickCount();

	ethernet_no_phy_fault.active = !SYS::Ethernet::phy.is_initialied();
	ethernet_no_link_fault.active = !SYS::Ethernet::phy.has_link();
	ethernet_no_server_fault.active = !SYS::Ethernet::log_tcp_client.is_connected();
	ethernet_client_connected.active = SYS::Ethernet::log_tcp_server.is_connected();

	adc_hardware_fault.active = !SYS::ADCTask::hw_ok();
	adc_event_fault.active = (xTaskGetTickCount() - SYS::ADCTask::last_event_tick) < 10000;

	lwl_data_fault.active = (SYS::LWL::key_data_error != 0);
	lwl_no_link_fault.active = !(SYS::LWL::has_link() || SYS::LWL::is_loopbacked());

	system_level_t next_level = (SYS::LWL::current_power > 0) ? ACTIVE : INACTIVE;


	auto fsrc = head_fault_source;
	bool any_change = false;

	while(fsrc) {
		if(fsrc->active && (fsrc->level > next_level))
			next_level = fsrc->level;

		if(fsrc->active != fsrc->was_active) {
			fsrc->was_active = fsrc->active;
			any_change = true;
		}

		fsrc = fsrc->next_source;
	}
	current_level = next_level;

	if(any_change || ((xTaskGetTickCount()-last_state_pub) > 5000)) {
		log_buffer_write_lock();
		log_puts_nowlock("STATE: ");

		switch(current_level) {
		case INACTIVE: log_puts_nowlock("READY"); break;
		case ACTIVE:   log_puts_nowlock("ACTIVE"); break;
		case EVENT:    log_puts_nowlock("TRIG"); break;
		case WARNING:  log_puts_nowlock("WARN"); break;
		case ERROR:    log_puts_nowlock("ERR"); break;
		}

		fsrc = head_fault_source;
		while(fsrc) {
			if(fsrc->active) {
				log_puts_nowlock(",");
				log_puts_nowlock(fsrc->item_key);
			}

			fsrc = fsrc->next_source;
		}

		log_puts_nowlock("\n");
		log_buffer_write_unlock();
		last_state_pub = xTaskGetTickCount();
	}
}

bool startup_ring() {
	if(xTaskGetTickCount() > 5000)
		return false;

	if((xTaskGetTickCount() > 1000) && (xTaskGetTickCount() < 4000)) {
		status_led.mode = Indicators::NONE;

		int start_sec = xTaskGetTickCount() / 1000 - 1;
		int start_ms  = xTaskGetTickCount() - 1000*(1+start_sec);

		if(start_ms < 600)
			status_led.color = Indicators::color_t::from_hsv(- 120 * start_sec);
		else if(start_ms >= 600 && start_ms < 750)
			status_led.color = Indicators::color_t::from_hsv(180 - 120*start_sec);
		else
			status_led.color = Indicators::color_t::from_u32(0);
	}
	else {
		status_led.mode = Indicators::NONE;
		status_led.color = Indicators::color_t::from_u32(0);
	}

	return true;
}

void recalculate_led() {
	static bool led_forced_off = false;

	if(led_forced_off) {
		status_led.mode = Indicators::HARD_ON;
		status_led = Indicators::color_t::from_u32(0);

		led_forced_off = false;

		return;
	}

	if(startup_ring())
		return;

	if(current_level == ERROR) {
		status_led.mode = Indicators::ALERT;
		status_led = Indicators::color_t::from_u32(Material::RED);

		return;
	}

	if((xTaskGetTickCount() - SYS::ADCTask::last_event_tick) < 75) {
		status_led.mode = Indicators::HARD_ON;
		status_led = Indicators::color_t::from_u32(Material::CYAN);

		led_forced_off = true;

		return;
	}

	if(current_level == WARNING) {
		status_led.mode = Indicators::WARN;
		status_led = Indicators::color_t::from_u32(Material::AMBER);

		return;
	}

	if(current_level == ACTIVE || current_level == EVENT)
		status_led.mode = Indicators::IDLE;
	else
		status_led.mode = Indicators::STANDBY;

	if(SYS::LWL::is_loopbacked())
		status_led = Indicators::color_t::from_u32(Material::PURPLE);
	else
		status_led = Indicators::color_t::from_u32(Material::GREEN);
}

void write_led()  {
	memset(led_data_buffer, 0, 13);

	status_led.draw_tick();
	status_led.write_rgb_data_to(led_data_buffer);

	HAL_SPI_Transmit_IT(&INDICATOR_SPI, led_data_buffer, 13);
}

void task_fn() {
	LOGI("Indicator", "Starting indicator thread");

	while(true) {
		check_state();

		recalculate_led();

		write_led();
		vTaskDelay((1000/portTICK_PERIOD_MS)/20);
	}
}

}
}

extern "C" void indicator_task_fn(void *arg) {
	SYS::Indicator::task_fn();
}
