/*
 * device_config.cpp
 *
 *  Created on: 21 Sep 2022
 *      Author: xaseiresh
 */

#include "config_data.h"
#include "main.h"

#include <stm32/flash/flash.h>
#include <all/log/log.h>

#include <cstring>

device_config_t device_cfg;

static const device_config_t fallback_config = {
		DEVICE_CONFIG_MAGIC_1,
		"FALLBACK",
		"c12d19fcca80",
		"testing",
		{
				{ 172, 16, 32, 1 },
				{ 255, 255, 255, 0 },
				{ 0x40, 0x12, 0x34, 0x56, 0x78, 0x9A },
				{ 172, 16, 32, 154 }
		},
		{ 172, 16, 32, 1 },
		9881,
		DEVICE_CONFIG_MAGIC_2
};

extern "C" void load_device_config() {
	memcpy(&device_cfg, reinterpret_cast<const uint8_t*>(DEVICE_CONFIG_LOCATION), sizeof(device_cfg));

	if(device_cfg._magic_1 != DEVICE_CONFIG_MAGIC_1
			|| device_cfg._magic_2 != DEVICE_CONFIG_MAGIC_2) {

		LOGW("CFG", "No device config present!");
		LOGW("CFG", "Fallback configuration used...");

		memcpy(&device_cfg, &fallback_config, sizeof(device_config_t));

		return;
	}

	LOGI("CFG", "Config loaded! I am %s!", device_cfg.name);
}

void save_device_config() {
	Flash::FlashWriter config_writer;

	config_writer.unlock();

	config_writer.begin(DEVICE_CONFIG_LOCATION,
			DEVICE_CONFIG_LOCATION + sizeof(device_config_t));
	config_writer.append(DEVICE_CONFIG_LOCATION, &device_cfg, sizeof(device_config_t));

	config_writer.lock();
}
