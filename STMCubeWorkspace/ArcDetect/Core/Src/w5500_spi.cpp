/*
 * w5500_spi.cpp
 *
 *  Created on: Sep 1, 2022
 *      Author: xaseiresh
 */

#include "main.h"

#include "w5500_spi.h"

#include <all/log/log.h>

extern "C" void spi_w5500_init() {
	SYS::w5500_spi_init();
}

namespace SYS {

SPI::Master spi5;

void spi5_done_fn(SPI_HandleTypeDef * spi) {
	spi5.task_notify_rw_done();
}
void spi5_send_rw(const void *tx, void *rx, size_t num) {
	uint8_t * tx_ptr = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(tx));
	uint8_t * rx_ptr = reinterpret_cast<uint8_t *>(rx);

	if(tx == nullptr && rx == nullptr) {
		spi5.task_notify_rw_done();
		return;
	}

	if(num < 32) {
		if(tx == nullptr) {
			HAL_SPI_Receive(&hspi5, rx_ptr, num, 1000);
		}
		else if(rx == nullptr) {
			HAL_SPI_Transmit(&hspi5, tx_ptr, num, 1000);
		}
		else {
			HAL_SPI_TransmitReceive(&hspi5, tx_ptr, rx_ptr, num, 1000);
		}

		spi5.task_notify_rw_done();
		return;
	}

	if(tx == nullptr) {
		HAL_SPI_Receive_DMA(&hspi5, rx_ptr, num);
	}
	else if(rx == nullptr) {
		HAL_SPI_Transmit_DMA(&hspi5, tx_ptr, num);
	}
	else {
		HAL_SPI_TransmitReceive_DMA(&hspi5, tx_ptr, rx_ptr, num);
	}


}

void w5500_spi_init() {
	LOGI("SYS", "Setting up W5500 SPI...");

	hspi5.TxRxCpltCallback = spi5_done_fn;
	hspi5.TxCpltCallback = spi5_done_fn;
	hspi5.RxCpltCallback = spi5_done_fn;

	spi5.init();

	spi5.set_rw_function(spi5_send_rw);

	uint8_t flush_data[1] = {};
	HAL_SPI_TransmitReceive_DMA(&hspi5, flush_data, flush_data, 1);

	LOGI("SYS", "W5500 SPI line configured!");
}
}
