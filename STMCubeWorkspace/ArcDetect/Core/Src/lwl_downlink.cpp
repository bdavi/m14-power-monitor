/*
 * lwl_downlink.cpp
 *
 *  Created on: Sep 26, 2022
 *      Author: xaseiresh
 */

#include "main.h"
#include "lwl_downlink.h"

#include <all/log/log.h>

// #define LWL_DISABLE_DOWNLINK_PING

#define LWL_GYROTRON_WARMUP_TIME_MS 200

namespace SYS {
namespace LWL {

uint8_t arc_fault_indicate = 0;

volatile int key_data_error_counter = 0;
volatile int key_data_error = 0;

bool send_keepalive = 0;
downlink_ping_data_t keepalive_data = {};

int received_keepalive = 0;
volatile int current_keepalive = 0;
volatile int keepalive_ticks = 0;

int received_power_ticks = 0;
bool received_power_upper_nibble = false;
uint8_t received_power = 0;
volatile __attribute__((section (".ccmram"))) int32_t current_power = 0;

volatile int gyrotron_warmup_ticks = 0;

void init() {
	USART6->CR1 |= USART_CR1_TE_Msk | USART_CR1_RE_Msk | USART_CR1_RXNEIE_Msk;

	LOGI("LWL", "Downlink initialized!");

#if defined(LWL_DISABLE_DOWNLINK_PING)
	LOGW("LWL", "Downlink heartbeat reporting modified, remove for production!");
#endif
}

void mark_arc() {
	// We need to leave the LSB unmodified so that the transmission code works properly (alternating between 170 and 0)
	arc_fault_indicate |= 0xE;

	USART6->CR1 |= USART_CR1_TXEIE_Msk;
}

void tick() {
	if(key_data_error_counter > 0)
		key_data_error_counter--;

	if(key_data_error_counter > 3000)
		key_data_error = 1000;

	if(key_data_error > 0)
		key_data_error--;

	current_keepalive = received_keepalive;
	received_keepalive = 0;

	if(current_keepalive == UP_KEEPALIVE)
		keepalive_ticks = 10;
	else if(keepalive_ticks > 0)
		keepalive_ticks--;

	if(received_power_ticks > 0)
		received_power_ticks--;
	else
		current_power = 100;

	if(current_power == 0)
		gyrotron_warmup_ticks = LWL_GYROTRON_WARMUP_TIME_MS;

	if(gyrotron_warmup_ticks > 0)
		gyrotron_warmup_ticks--;

	keepalive_data = {
			DOWN_KEEPALIVE,
			(keepalive_ticks > 0) && (received_power_ticks > 0),
			1,
			1,
			1
	};

#ifndef LWL_DISABLE_DOWNLINK_PING
	send_keepalive = true;
	USART6->CR1 |= USART_CR1_TXEIE_Msk;
#endif
}

inline void isr_handler() {
	uint8_t usart_sr = USART6->SR;
	uint8_t usart_data = USART6->DR;

	if(((usart_sr & USART_SR_RXNE_Msk) != 0)) {

		// These flags can all indicate that a UART Message was not received properly.
		// The FE triggers on desynchronization of the frame, the NE when potential noise was
		// detected on the line. The PE is currently unused (always 0), but if Parity Bits
		// were enabled, this could add additional error detection
		if(((usart_sr & (USART_SR_NE_Msk | USART_SR_FE_Msk | USART_SR_PE_Msk)) != 0)) {
			if(key_data_error_counter < 5000)
				key_data_error_counter += 500;

			received_power_upper_nibble = false;
		}
		else {
			uint8_t data_key = usart_data & 0xF;
			uint8_t data_payload = (usart_data & 0xF0) >> 4;

			switch(uplink_keys_t(data_key)) {
			default:
				if(key_data_error_counter < 5000)
					key_data_error_counter += 500;
			break;
			case UP_LOOPBACK:
			case UP_KEEPALIVE:
				received_keepalive = data_key;
			break;
			case UP_POWER_HIGH:
				received_power = data_payload << 4;
				received_power_upper_nibble = true;
			break;
			case UP_POWER_LOW:
				if(received_power_upper_nibble) {
					received_power |= data_payload;

					if(received_power == 0)
						gyrotron_warmup_ticks = LWL_GYROTRON_WARMUP_TIME_MS;

					current_power = received_power;

					received_power_ticks = 1000;
				}
				received_power_upper_nibble = false;

			break;
			}
		}
	}

	if((usart_sr & USART_SR_TXE_Msk) && (USART6->CR1 & (USART_CR1_TXEIE_Msk))) {
		if(arc_fault_indicate > 0) {
			USART6->DR = (arc_fault_indicate & 1) ? DOWN_ARC_DETECTED_1 : DOWN_ARC_DETECTED_0;
			arc_fault_indicate--;
		}
		else if(send_keepalive) {
			send_keepalive = false;
			USART6->DR = *reinterpret_cast<uint8_t*>(&keepalive_data);
		}
		else {
			USART6->CR1 &= ~(USART_CR1_TXEIE_Msk);
		}
	}
}

bool has_link() {
	return keepalive_ticks > 0;
}
bool is_loopbacked() {
	return current_keepalive == UP_LOOPBACK;
}

}
}

extern "C" void lwl_downlink_init() {
	SYS::LWL::init();
}
extern "C" void lwl_downlink_tick() {
	SYS::LWL::tick();
}

extern "C" void USART6_IRQHandler() {
	SYS::LWL::isr_handler();
}
