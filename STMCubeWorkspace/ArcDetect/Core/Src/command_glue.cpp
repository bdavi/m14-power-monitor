/*
 * command_glue.cpp
 *
 *  Created on: 3 Nov 2022
 *      Author: xaseiresh
 */

#include "command_glue.h"

#include "main.h"

#include "ethernet_impl.h"
#include "config_data.h"
#include "adc_isr.h"


#include <all/log/log.h>

#include <cstring>
#include <stdlib.h>
#include <stdio.h>

namespace SYS {
namespace CMD {

CommandBuffer log_server_buffer;
CommandBuffer log_client_buffer;

void print_ip_piece(const char * piece, const uint8_t * ip) {
	log_printf_nowlock("CFG: %s %d.%d.%d.%d\n", piece, ip[0], ip[1], ip[2], ip[3]);
}

void print_config() {
	log_buffer_write_lock();

	log_printf_nowlock("CFG: DEVICE NAME: %s\n", device_cfg.name);

	log_puts_nowlock("CFG: --- ETH CONFIG\n");
	auto mac_ptr = device_cfg.own_addr_config.source_hw_address;
	log_printf_nowlock("CFG: MAC %02X:%02X:%02X:%02X:%02X:%02X\n",
		mac_ptr[0], mac_ptr[1], mac_ptr[2], mac_ptr[3], mac_ptr[4], mac_ptr[5]);
	print_ip_piece("IP", device_cfg.own_addr_config.source_ip);
	print_ip_piece("SUBNET", device_cfg.own_addr_config.subnet_mask);
	print_ip_piece("GATEWAY", device_cfg.own_addr_config.gateway);

	log_printf_nowlock("CFG: --- SERVER CONFIG\nCFG: SERVER ADDR %d.%d.%d.%d:%d\n",
		device_cfg.main_server_ip[0], device_cfg.main_server_ip[1], device_cfg.main_server_ip[2], device_cfg.main_server_ip[3],
		device_cfg.main_server_port);

	log_buffer_write_unlock();
}

void on_command_fn(CommandBuffer & cmd) {
	if(cmd.cmd_equal("RESET") || cmd.cmd_equal("RESTART") || cmd.cmd_equal("REBOOT")) {
		NVIC_SystemReset();
	}
	else if(cmd.cmd_equal("TRIG")) {
		Sampler::force_measurement();

		cmd.puts("OK");
	}
	else if(cmd.cmd_equal("TRIG_FAKE_ARC")) {
		Sampler::normalized_power_smoothed = 20000 << 16;
		cmd.puts("OK");
	}
	else if(cmd.cmd_equal("TSYNC!")) {
		char tsbuf[32] = {};
		snprintf(tsbuf, 32, "%lu ", xTaskGetTickCount());
		cmd.puts(tsbuf);
		cmd.puts(cmd.get_arg(0), ' ');
	}
	else if(cmd.cmd_equal("SET_IP")) set_ipcfg_item(cmd, device_cfg.own_addr_config.source_ip);
	else if(cmd.cmd_equal("SET_GATEWAY")) set_ipcfg_item(cmd, device_cfg.own_addr_config.gateway);
	else if(cmd.cmd_equal("SET_SUBNET")) set_ipcfg_item(cmd, device_cfg.own_addr_config.subnet_mask);
	else if(cmd.cmd_equal("SET_SERVER")) set_ipcfg_item(cmd, device_cfg.main_server_ip);
	else if(cmd.cmd_equal("SET_MAC")) {
		uint8_t target_mac[6] = {};

		for(int i=0; i<6; i++) {
			long next_addr = 0;
			if(!cmd.parse_long(i, next_addr)) {
				cmd.puts("Specify MAC as xxx xxx xxx xxx xxx xxx!");
				return;
			}
			if(next_addr < 0 || next_addr > 255) {
				cmd.puts("Invalid range for IP address element!");
				return;
			}

			target_mac[i] = next_addr;
		}

		memcpy(device_cfg.own_addr_config.source_hw_address, target_mac, 6);
		cmd.puts("OK");
	}
	else if(cmd.cmd_equal("SET_DEVICE_NAME")) {
		auto ptr = cmd.get_arg(0);
		if(ptr == nullptr)
			cmd.puts("No name given!");
		else if(strlen(ptr) > 16)
			cmd.puts("Name must be shorter than 16 characters!");
		else if(strchr(ptr, ' ') != nullptr) {
			cmd.puts("Name must not contain spaces!");
		}
		else {
			strncpy(device_cfg.name, cmd.get_arg(0), 16);
			cmd.puts("OK");
		}
	}
	else if(cmd.cmd_equal("SAVE_CFG")) {
		save_device_config();
		cmd.puts("OK");
	}
	else if(cmd.cmd_equal("GET_CFG")) {
		cmd.puts("OK");
		print_config();
	}
	else
		cmd.puts("UNKNOWN CMD");
}

void set_ipcfg_item(CommandBuffer & cmd, uint8_t * ptr) {
	uint8_t target_ip[4] = {};

	for(int i=0; i<4; i++) {
		long next_addr = 0;
		if(!cmd.parse_long(i, next_addr)) {
			cmd.puts("Specify IP as xxx xxx xxx xxx!");
			return;
		}
		if(next_addr < 0 || next_addr > 255) {
			cmd.puts("Invalid range for IP address element!");
			return;
		}

		target_ip[i] = next_addr;
	}

	memcpy(ptr, target_ip, 4);
	cmd.puts("OK");
}


void init() {
	log_server_buffer.cmd_puts_fn = log_puts;
	log_server_buffer.cmd_exec_fn = on_command_fn;

	log_client_buffer.cmd_puts_fn = log_puts;
	log_client_buffer.cmd_exec_fn = on_command_fn;
}

}
}
