/*
 * w5500_thread.cpp
 *
 *  Created on: 2 Sep 2022
 *      Author: xaseiresh
 */

#include "main.h"

#include "w5500_spi.h"
#include "ethernet_impl.h"

#include "config_data.h"

#include "adc_isr.h"

#include "command_glue.h"

#include <all/log/log.h>

#include <stm32/flash/flash.h>

#include <cstring>
#include <stdlib.h>
#include <stdio.h>

namespace SYS {
namespace Ethernet {

SPI::Device w5500_spi(spi5, &(GPIOD->ODR), 0);

W5500::Core phy(w5500_spi);
W5500::BaseSocket log_tcp_server(phy);
W5500::BaseSocket log_tcp_client(phy);

void task() __attribute__((__noreturn__));

char transfer_buffer[256] = {};

void on_socket_event(void *arg, W5500::BaseSocket::socket_event_t evt, W5500::BaseSocket & sock, CommandBuffer & bfr) {
	if(evt == W5500::BaseSocket::CONNECTED) {
		static char intro_buffer[256] = {};

		snprintf(intro_buffer, 255, "\nCONNECT NAME=%s TYPE=ArcDetect MODE=APP BRANCH=%s VERSION=%ld\n",
				device_cfg.name,
				device_cfg.branch,
				MAIN_PROGRAM_CFG->version_nr);

		sock.dump_tx(intro_buffer, strlen(intro_buffer));
	}
	else if(evt == W5500::BaseSocket::RX_COMPLETE) {
		size_t rx_avail = sock.get_rx_available();

		while(rx_avail) {
			size_t transfer_size = std::min<size_t>(sizeof(transfer_buffer)-1, rx_avail);
			sock.peek_rx(transfer_buffer, transfer_size);
			sock.free_rx(transfer_size);
			transfer_buffer[transfer_size] = 0;

			bfr.feed(transfer_buffer);

			rx_avail = sock.get_rx_available();
		}
	}
}

void log_client_on_event(void * arg, W5500::BaseSocket::socket_event_t evt) {
	on_socket_event(arg, evt, log_tcp_client, CMD::log_client_buffer);
}

void log_server_on_event(void * arg, W5500::BaseSocket::socket_event_t evt) {
	on_socket_event(arg, evt, log_tcp_server, CMD::log_server_buffer);
}

TickType_t last_connected_tick = 0;
void push_logs(const char *c, size_t len) {
	while(1) {
		if(log_tcp_server.is_connected())
			break;
		if(log_tcp_client.is_connected())
			break;

		if((xTaskGetTickCount() - last_connected_tick) > 10000/portTICK_PERIOD_MS)
			return;

		vTaskDelay(50/portTICK_PERIOD_MS);
	}
	last_connected_tick = xTaskGetTickCount();

	log_tcp_server.dump_tx(c, len);
	log_tcp_client.dump_tx(c, len);
}

void task() {
	LOGI("SYS", "W5500 Ethernet task starting up...");
	phy.init();
	phy.enable_int();

	EXTI->IMR |= 1<<1;

	W5500::BaseSocket::socket_config_t log_sock_cfg = {};
	log_sock_cfg.source_port = 22;
	log_sock_cfg.mode = W5500::BaseSocket::TCP_SERVER;

	log_tcp_server.on_event_cb = log_server_on_event;

	log_tcp_server.start(log_sock_cfg);
	LOGI("SYS", "Logging & status port opened.");

	phy.configure_address(device_cfg.own_addr_config);

	W5500::BaseSocket::socket_config_t log_client_cfg = {};
	log_client_cfg.mode = W5500::BaseSocket::TCP_CLIENT;
	log_client_cfg.source_port = 39947;
	log_client_cfg.dest_port = device_cfg.main_server_port;
	memcpy(log_client_cfg.dest_ip, device_cfg.main_server_ip, 4);

	log_tcp_client.on_event_cb = log_client_on_event;

	log_tcp_client.start(log_client_cfg);
	LOGI("SYS", "Logging client opened");

	CMD::init();

	phy.loop_isr();
}

}
}

extern "C" void ethernet_push_logs(const char *c, size_t len) {
	SYS::Ethernet::push_logs(c, len);
}
extern "C" void ethernet_task_fn(void *arg) {
	SYS::Ethernet::task();
}
extern "C" void EXTI1_IRQHandler(void) {
	EXTI->PR |= 1<<1;
	SYS::Ethernet::phy.notify_from_isr();
}
