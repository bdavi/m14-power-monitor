/*
 * adc_isr.cpp
 *
 *  Created on: Sep 9, 2022
 *      Author: xaseiresh
 */

#include "main.h"
#include <array>

#include "adc_isr.h"
#include "adc_task.h"

#include "lwl_downlink.h"

#pragma GCC push_options
#pragma GCC optimize("Ofast")

namespace Sampler {

// Predefinitions
void monitoring_eval();
void monitoring_config_pga();

// Fixed settings for the PGA11x. Do not change CMD_Type or Gain Select. Only change chsel if necessary
static const pga_11x_packet_t measurement_pga_config = {
		.chsel = 4,
		.gsel = 0,
		.cmd_type = 0b00111010,
};
static const pga_11x_packet_t selftest_configs[] = {
		{ .chsel = 0, .gsel = 0, .cmd_type = 0b00111010 },	// Set to external VCal (4.096V)
		{ .chsel = 12, .gsel = 0, .cmd_type = 0b00111010 },	// To GND
		{ .chsel = 13, .gsel = 0, .cmd_type = 0b00111010 },	// To 90% VCal
		{ .chsel = 14, .gsel = 0, .cmd_type = 0b00111010 },	// 10% VCal
		{ .chsel = 15, .gsel = 0, .cmd_type = 0b00111010 },	// External 2.048 Ref
};

// Zero adjustment value, to be added to the adc_data value before scaling it.
// Set by the ADC Task Tick Hook routine
CCM_DATA volatile int32_t adc_zero_adjust = 0;

// 2ms PT1-Smoothed ADC data, used by the ADC Task to log current microwave power levels
CCM_DATA volatile int32_t adc_smoothed = 0;
CCM_DATA volatile int32_t adc_noise = 0;
// Exported variable, used by the ADC Task to also log expected ADC levels for comparison
CCM_DATA volatile int32_t power_detect_expected_adc = 0;

// Internal temporary variables, used by the system to create smoothed out reference
// values for anomaly detection and power loss detection
CCM_DATA int32_t anomaly_detect_smoothed = 0;
CCM_DATA int32_t normalized_power_smoothed = 0;

// Definition of the trigger condition. Essentially checks if the ADC value deviates more than ADC_ANOMALY_TRIGGER_THRESHOLD from the expected value
#define ADC_SAMPLE_TRIG_COND ((adc_data > (ADC_ANOMALY_TRIGGER_THRESHOLD + (anomaly_detect_expected_adc))) || (adc_data < (anomaly_detect_expected_adc - ADC_ANOMALY_TRIGGER_THRESHOLD)))

// Circular sample buffer. Data will be filled into the buffer in "backwards" order, i.e.
// from 2047 down to 0. Data will also stop at an arbitrary position indicated by sample_buffer_end
CCM_DATA sample_data_t sample_buffer[ADC_ANOMALY_SAMPLE_BUFFER_SIZE] = {};

// End of the circular buffer. Read data in sample_buffer[] from here in reverse order
CCM_DATA volatile int32_t  sample_buffer_end = 1;
// FreeRTOS Tick at which the sample was triggered
CCM_DATA volatile uint32_t sample_buffer_trigger_tick = 0;

// Flag indicating that an arc was detected. 
// This value, together with arc_end_tick, are used by the ADC task to determine when an arc finished.
// This information will be transmitted down to the database for a visual Grafana marker
CCM_DATA volatile bool     arc_detect_flag = false;
// FreeRTOS start tick of arc detection
CCM_DATA volatile uint32_t arc_start_tick = 0;
// FreeRTOS tick of last arc detection//when it ended
CCM_DATA volatile uint32_t arc_end_tick   = 0;

// State of the sampling system. The sampler will remain in LOCKED until it is 
// forced into state RESTART by an external code. From there it will go into PRESAMPLING,
// then into WAIT_FOR_TRIG, where it will wait for the ADC trigger condition to hold true
// for ADC_ANOMALY_TRIGGER_COUNT ADC ticks. It will then fill up the remaining 
// sample_buffer[] space, before going into LOCKED.
CCM_DATA volatile sample_buffer_state_t sample_buffer_state = RESTART;
// Internal counter of the sample buffer system
CCM_DATA volatile uint32_t sample_buffer_state_ctr = 0;

// Selftest value counter. Contains the raw selftest results, 
// evaluated by the ADC task
volatile uint16_t  selftest_buffer[5*16] = {};

// Internal counter variables of the selftesting system
CCM_DATA volatile uint32_t selftest_config_ctr = 0;
CCM_DATA volatile uint32_t selftest_sample_ctr  = 0;

void no_op() { }
void (*active_sampler_function)(void) = no_op;

CCM_DATA int32_t sequence_counter = 0;

// Internal variable that holds the raw value read out 
// from the ADC SPI line
CCM_DATA int32_t adc_data = 0;

inline void trigger_adc_task() {
	// Triggering can't be done directly, as the
	// ADC ISR priority is stronger than the FreeRTOS one.
	// An EXTI Line, EXTI 5, is used to generate
	// a lower priority interrupt. Setting the SWIER bit
	// triggers an interrupt at lesser priority, which 
	// can then call needed FreeRTOS functions

	EXTI->SWIER |= 1<<5;
}

// This function runs the ADC through a selftest cycle. It will switch
// out the PGA MUX settings, wait for approx 480 ticks, take 16 samples of 
// data which are stored in the selftest_buffer, and then return control 
// to the ADC monitoring function.
void selftest_fn() {
	if(selftest_sample_ctr == 0) {
		GPIOC->ODR &= ~(1<<7); // Pull CS line low
		SPI3->DR  = *reinterpret_cast<const uint16_t*>(&selftest_configs[selftest_config_ctr]);
	}
	else if(selftest_sample_ctr == 10) {
		GPIOC->ODR |= (1<<7); // De-assert PGA CS LINE
	}
	// This long of a delay is needed to let the PGA line stabilize,
	// as it takes a while for some of the MUX switching effects to go away
	else if((selftest_sample_ctr >= 480) && (selftest_sample_ctr < (480+16))) {
		selftest_buffer[selftest_sample_ctr - 480 + 16*selftest_config_ctr] = adc_data;
	}

	if(selftest_sample_ctr == (480+16)) {
		selftest_config_ctr++;
		selftest_sample_ctr = 0;

		if(selftest_config_ctr == (sizeof(selftest_configs)/2)) {
			trigger_adc_task();
			active_sampler_function = monitoring_config_pga;
		}
	}
	else
		selftest_sample_ctr++;
}

// This function is part of the standard ADC monitoring
// state-machine, and initialises the PGA by setting it
// to specified default parameters.
// It will then pass functionality on to the standard
// ADC measure&evaluate function
void monitoring_config_pga() {
	switch(sequence_counter) {
	default: break;
	case 0:
		GPIOC->ODR &= ~(1<<7); // Pull CS line low
		SPI3->DR  = *reinterpret_cast<const uint16_t*>(&measurement_pga_config);
	break;
	case 9:
		GPIOC->ODR |= (1<<7); // De-assert PGA CS LINE
	break;
	case 100:
		// Trigger the ADC to sample and hand off to next function ptr
		sequence_counter = 0;
		active_sampler_function = monitoring_eval;
	return;
	}

	sequence_counter++;
}

void monitoring_eval() {
	CCM_DATA static int arc_trigger_counter = 0;

	// For accurate scaling, the data must be zeroed out. 
	// adc_zero_adjust is generated by the ADC Tasks's Tick Hook
	adc_data = adc_data - (ADC_DATA_CENTER + adc_zero_adjust);

	// Smooth out the ADC over the last ca. 255 samples to provide
	// cleaner readings for the ADC Task's Tick Hook to process
	int32_t adc_smoothed_diff = (adc_data<<8) - (adc_smoothed>>8);
	adc_smoothed += adc_smoothed_diff;
	if(adc_noise > 0)
		adc_noise--;
	if(adc_smoothed_diff < 0)
		adc_smoothed_diff = -adc_smoothed_diff;
	if(adc_smoothed_diff > adc_noise)
		adc_noise = adc_smoothed_diff;

	int32_t anomaly_detect_expected_adc = 0;
	power_detect_expected_adc = 0;

	// Max range for adc_data: 0 to 16384, centred data roughly -+8192. *100 means +-819200, or .07% of signed 32 bit full scale.
	// Let's assume worst case, i.e. current_power = 10 (otherwise the kickstart code kicks in, so
	// power proportionality has +-81920, but we need to limit to +-16384 to comply with the smoothing stage.
	// This will not normally cause issues, as current_power should also be somewhere in the 100-255 range for full scale
	// ADC readings.
	//
	// No other stages down the line multiply in dangerous ways.
	int32_t power_proportionality = 0;

	// Keep triggering fully off, since there's nothing
	// to even record. This disables anomaly and power loss detection
	// Note: gyrotron_warmup_ticks is set to 20 while current_power == 0, and 
	// is reduced by 1 every FreeRTOS tick, i.e. the detection system remains
	// off for the first 20ms to let the gyrotron output stabilize
	if((SYS::LWL::current_power == 0) || (SYS::LWL::gyrotron_warmup_ticks > 0)) {
		// Fixate these values to the current is-values to force detections off
		anomaly_detect_expected_adc = (adc_data);
		power_detect_expected_adc = 0;

		if(SYS::LWL::current_power != 0) {
			power_proportionality = (((adc_smoothed)>>16)*100) / SYS::LWL::current_power;
		}
		if(power_proportionality >= 32760)
			power_proportionality = 32760;
		else if(power_proportionality <= -32760)
			power_proportionality = -32760;

		// Safe, power_proportionality is limited above
		// We also want to keep these up to date, so that when we 
		// do enable our detections, their thresholds are set correctly
		anomaly_detect_smoothed = power_proportionality * (ADC_ANOMALY_SMOOTHING_FACTOR);
		normalized_power_smoothed = power_proportionality * (ADC_ARCING_SMOOTHING_FACTOR);
	}
	else {
		power_proportionality = ((adc_data)*100) / SYS::LWL::current_power;

		if(power_proportionality >= 32760)
			power_proportionality = 32760;
		else if(power_proportionality <= -32760)
			power_proportionality = -32760;

		// Smoothing code, acts like a PT1 element over the last 2^14 samples, i.e. about 32ms
		const int32_t anomaly_detect_diff = (power_proportionality) - (anomaly_detect_smoothed / (ADC_ANOMALY_SMOOTHING_FACTOR));
		anomaly_detect_smoothed += anomaly_detect_diff;

		// Safe, anomaly_detect_smoothed>>16 gives enough room for the at-highest "255" current_power reading.
		anomaly_detect_expected_adc = (((anomaly_detect_smoothed / (ADC_ANOMALY_SMOOTHING_FACTOR)) * SYS::LWL::current_power) / 100);

		// Same smoothing code as above. This must be separate, because our anomaly_detect_smoothed value
		// is actively reset to a different value (to avoid triggers with no event in them), whereas
		// our power detection must remain unaffected by this adjustment!
		const int32_t normalized_power_diff = (power_proportionality) - (normalized_power_smoothed / (ADC_ARCING_SMOOTHING_FACTOR));
		normalized_power_smoothed += normalized_power_diff;

		// Same here as the above note
		power_detect_expected_adc = (((normalized_power_smoothed / (ADC_ARCING_SMOOTHING_FACTOR)) * SYS::LWL::current_power) / 100);

		if(power_detect_expected_adc > ADC_ARCING_MINIMUM_AMPLITUDE || power_detect_expected_adc < -ADC_ARCING_MINIMUM_AMPLITUDE) {
			// Safe because we check the expected ADC reading above.
			// In case our expected power reading is too low, we don't do anything and don't arm, i.e.
			// the readouts can't be trusted.

			// normalized_power is a fixcomma value showing the percentage of our current power level
			// compared to the expected power level. 
			const int32_t normalized_power = ((adc_data) * int32_t(1<<16)) / power_detect_expected_adc;

			// If we are below a certain power percentage compared to our expected level, we have a power
			// loss and need to trigger
			if(normalized_power < ADC_ARCING_THRESHOLD_LEVEL) {
				arc_trigger_counter++;

				if(arc_trigger_counter > ADC_ARCING_THRESHOLD_COUNTER) {
					arc_trigger_counter = ADC_ARCING_THRESHOLD_COUNTER;

					SYS::LWL::mark_arc();

					arc_end_tick = SYS::ADCTask::adc_meas_tick;
					if(!arc_detect_flag) {
						arc_start_tick = SYS::ADCTask::adc_meas_tick;
						arc_detect_flag = true;

						if(sample_buffer_state == WAIT_FOR_TRIG) {
							sample_buffer_state = POSTSAMPLING;
							sample_buffer_state_ctr = 2047-ADC_ANOMALY_PRESAMPLING_COUNT;
							sample_buffer_trigger_tick = SYS::ADCTask::adc_meas_tick;
						}
					}
				}
			}
			else
				arc_trigger_counter = 0;
		}
	}


	// We only need to save samples into our measurement buffer if we are actively sampling.
	// Never save samples in other states, as this might corrupt the readout of the ADC Task, as
	// it expects the buffer and sample_end_pos to remain unaffected.
	if(sample_buffer_state >= PRESAMPLING && sample_buffer_state <= POSTSAMPLING) {
		sample_buffer_end--;
		sample_buffer[sample_buffer_end].adc_is = adc_data;

#ifdef ADC_ISR_SEND_CURRENT_POWER
		sample_buffer[sample_buffer_end].adc_exp = SYS::LWL::current_power; // power_detect_expected_adc;
#else
		sample_buffer[sample_buffer_end].adc_exp = power_detect_expected_adc;
#endif
		if(sample_buffer_end == 0) {
			sample_buffer_end = ADC_ANOMALY_SAMPLE_BUFFER_SIZE;
		}
	}

	switch(sample_buffer_state) {
	case LOCKED: break;	// Don't do anything until we're told to
	case RESTART:	// Set up everything as needed for the rest

		sample_buffer_state_ctr = ADC_ANOMALY_PRESAMPLING_COUNT;
		sample_buffer_state = PRESAMPLING;

	// Intentional fall-through to the presampling stage
	case PRESAMPLING:	// Will essentially wait for ADC_ANOMALY_PRESAMPLING_COUNT samples
		if(--sample_buffer_state_ctr == 0) {
			// This will force more delta before a trigger can occur.
			// Doing this avoids multiple "uninteresting" triggers after a step change
			anomaly_detect_smoothed = power_proportionality * (ADC_ANOMALY_SMOOTHING_FACTOR);

			sample_buffer_state = WAIT_FOR_TRIG;
			sample_buffer_state_ctr = 0;
		}
		break;
	case WAIT_FOR_TRIG:
		if(ADC_SAMPLE_TRIG_COND) {
			sample_buffer_state_ctr++;
			// Only trigger if we had enough samples fulfilling our trigger 
			// condition. We have noisy sensors, this helps.
			if(sample_buffer_state_ctr >= ADC_ANOMALY_TRIGGER_COUNT) {
				sample_buffer_state = POSTSAMPLING;
				sample_buffer_state_ctr = 2047-ADC_ANOMALY_PRESAMPLING_COUNT;
				sample_buffer_trigger_tick = SYS::ADCTask::adc_meas_tick;
			}
		}
		else {
			sample_buffer_state_ctr = 0;
		}

		break;
	case POSTSAMPLING:
		// Finish up sampling, i.e. fill up the remaining circular sample buffer
		// then pass buffer control over to the ADC task
		if(--sample_buffer_state_ctr == 0) {
			sample_buffer_state = LOCKED;
			trigger_adc_task();
		}
	}
}

void init() {
	// We need the EXTI9_5 IRQ for the ADC ISR -> ADC Task signalling
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 6, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	EXTI->IMR |= 1<<5; // Enable EXTI line 5

	SPI3->CR1 |= SPI_CR1_SPE_Msk;
	SPI1->CR1 |= SPI_CR1_SPE_Msk;

	GPIOC->ODR |= (1<<7); // Release PGA CS Line

	TIM2->ARR   = 48;


	TIM2->DIER = 0;

	sequence_counter = 0;
	active_sampler_function = monitoring_config_pga;

	TIM2->DIER = TIM_DIER_UIE_Msk;

	TIM2->CR1 |= TIM_CR1_CEN_Msk | TIM_CR1_OPM_Msk;
}

void trigger_selftest() {
	selftest_sample_ctr = 0;
	selftest_config_ctr = 0;

	active_sampler_function = selftest_fn;

	// TIM2->DIER = TIM_DIER_UIE_Msk;
	// TIM2->CR1 |= TIM_CR1_CEN_Msk;
}
void clear_selftest() {
	selftest_sample_ctr = 0;
	selftest_config_ctr = 0;
}
bool selftest_done() {
	return selftest_config_ctr == sizeof(selftest_configs)/2;
}

void restart_sampler() {
	sample_buffer_state = sample_buffer_state_t::RESTART;
}
bool sample_available() {
	return sample_buffer_state == sample_buffer_state_t::LOCKED;
}
void force_measurement() {
	// Forces the ADC Sampler into a full buffer save by
	// setting it into the POSTSAMPLING mode, which will
	// unconditionally save data and then signal the ADC Task

	sample_buffer_trigger_tick = SYS::ADCTask::adc_meas_tick;
	sample_buffer_state_ctr = 2048;
	sample_buffer_state = sample_buffer_state_t::POSTSAMPLING;
}

}


extern "C" void EXTI9_5_IRQHandler(void) {
	if((EXTI->PR & (1<<5)) != 0) {
		SYS::ADCTask::trigger_task_from_isr();
	}

	// Clear all pending ISRs
	EXTI->PR |= 0b11111 << 5;
}

extern "C" void TIM2_IRQHandler(void) {
	// Clear all timer-related interrupt flags
	TIM2->SR = 0;

	ADC_CS_GPIO_Port->ODR |= ADC_CS_Pin;
	Sampler::adc_data = SPI1->DR >> 1;

	// Pass functionality over to the specific code.
	// Using a function pointer makes for a more efficient
	// state-machine operation
	Sampler::active_sampler_function();

	ADC_CS_GPIO_Port->ODR &= ~ADC_CS_Pin;
	SPI1->DR = 0;

	TIM2->CR1 |= TIM_CR1_CEN_Msk;
}
extern "C" void sampler_init(void) {
	Sampler::init();
}

#pragma GCC pop_options
