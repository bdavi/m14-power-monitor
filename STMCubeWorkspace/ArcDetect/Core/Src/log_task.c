/*
 * log_task.cpp
 *
 *  Created on: Sep 1, 2022
 *      Author: xaseiresh
 */

#include "main.h"

#include <all/log/log.h>

#include <FreeRTOS.h>
#include <task.h>

#include <stddef.h>

void ethernet_push_logs(const char *c, size_t len);

#define LOG_UART huart3

static const char * log_output_buffer_ptr;
static size_t log_output_buffer_len;

static TaskHandle_t log_task_handle = 0;

static int  uart_log_busy = 0;
void usr_log_uart_complete_cb(struct __UART_HandleTypeDef *huart) {
	uart_log_busy = 0;

	if(log_task_handle) {
		xTaskNotifyFromISR(log_task_handle, 0, eNoAction, 0);
		portYIELD();
	}
}
void usr_log_uart_start(const char * ptr, size_t len) {
	uart_log_busy = 1;
	HAL_UART_Transmit_IT(&LOG_UART, (uint8_t*)ptr, len);
}
void usr_log_uart_wait_complete() {
	while(uart_log_busy)
		xTaskNotifyWait(0, 0, 0, portMAX_DELAY);
}
void usr_log_uart_setup() {
	LOG_UART.TxCpltCallback = usr_log_uart_complete_cb;
}

volatile uint32_t transferred_log_buf_size = 0;
void usr_log_task_fn(void * args) {

	log_task_handle = xTaskGetCurrentTaskHandle();

	usr_log_uart_setup();

	while(1) {

		log_output_buffer_len = 1024;

		log_take_buffer(&log_output_buffer_len, &log_output_buffer_ptr);
		transferred_log_buf_size = log_output_buffer_len;

		// usr_log_uart_start(log_output_buffer_ptr, log_output_buffer_len);

		ethernet_push_logs(log_output_buffer_ptr, log_output_buffer_len);
		usr_log_uart_wait_complete();

		log_free_buffer(log_output_buffer_len);
		vTaskDelay(2);
	}
}
