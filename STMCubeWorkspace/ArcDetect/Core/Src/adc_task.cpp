/*
 * adc_task.cpp
 *
 *  Created on: Sep 12, 2022
 *      Author: xaseiresh
 */

#include "adc_isr.h"
#include "lwl_downlink.h"

#include <FreeRTOS.h>
#include <task.h>

#define LOG_LEVEL LOG_DEBUG
#include <all/log/log.h>

#include <all/log/log_datalogger.h>

#include <cmath>
#include <cstring>
#include <algorithm>

namespace SYS {
namespace ADCTask {

Log::Datalogger<int16_t, 2048, 4> test_data_buffer("ADC/AVG");

TickType_t last_event_tick = 0;
TickType_t adc_meas_tick = 0;

// FreeRTOS Handle of the ADC Task, used for signalling
TaskHandle_t task_handle = nullptr;

// FreeRTOS Tick of the last selftest. Used to space out tests
TickType_t last_selftest = 0;

// Expected values of the different channels
const uint16_t selftest_target_values[] = {
		16364,
		40,
		14745,
		1638,
		8192
};
// Expected maximum standard deviations of the selftests
const float selftest_max_std_dev[] = {
		20.0F,
		20.0F,
		5.0F,
		5.0F,
		5.0F
};

struct selftest_result_t {
	int32_t ch_avg;
	float   ch_std_dev;
	int32_t ch_error;
};

selftest_result_t selftest_calculations[5] = {};
bool hw_is_ok = false;

bool run_selftest() {
	Sampler::clear_selftest();
	Sampler::trigger_selftest();

	while(!Sampler::selftest_done())
		ulTaskNotifyTake(1, 10);

	bool ok = true;

	for(int i=0; i<5; i++) {
		int32_t ch_avg = 0;
		for(int s=0; s<16; s++)
			ch_avg += Sampler::selftest_buffer[s+16*i];
		ch_avg /= 16;

		int32_t ch_std_dev = 0;
		for(int s=0; s<16; s++) {
			int32_t d = Sampler::selftest_buffer[s+16*i] - ch_avg;

			ch_std_dev += d*d;
		}
		selftest_result_t result = {
				ch_avg, sqrtf(ch_std_dev)/16, ch_avg - int32_t(selftest_target_values[i])
		};
		selftest_calculations[i] = result;

		if(fabsf(result.ch_error) > 50) {
			LOGE("ADC", "Channel %d shows high error (%d bit)", i, result.ch_error);
			ok = false;
		}
		if(result.ch_std_dev > selftest_max_std_dev[i]) {
			LOGE("ADC", "Channel %d shows high std. error (%d mbit)", i, int(result.ch_std_dev*1000));
			ok = false;
		}
	}

	hw_is_ok = ok;

	return ok;
}

int graphing_lines_buffer[130] = {};
char graphing_char_buffer[130*3 + 1] = {};

#define SAMP_N(n) Sampler::sample_buffer[(Sampler::sample_buffer_end + 1024 + 256 - (n*512)/130) & 2047].adc_exp

// This function is currently unused, but still quite useful
// It will print a console-readable graphical representation of the last 
// recorded sample, for quick verification of read data when a server
// is not available
void graph_sample() {
	int draw_max = -16384;
	int draw_min = 16384;

	for(int i=0; i<130; i++) {
		auto s = SAMP_N(i);

		draw_max = (draw_max > s) ? draw_max : s;
		draw_min = (draw_min < s) ? draw_min : s;
	}

	LOGI("ADC/B64", "Min/Max %d %d", draw_min, draw_max);

	log_buffer_write_lock();

	log_puts_nowlock("ADC Graph: START\n");


	for(int i=0; i<130; i++) {
		auto s = SAMP_N(i);

		graphing_lines_buffer[i] = (8*20*(s-draw_min))/(draw_max-draw_min);
	}

	for(int i=19; i>=0; i--) {
		log_puts_nowlock("ADC Graph: ");
		char * graph_char_ptr = graphing_char_buffer;

		for(int j=0; j<130; j++) {

			char blockcode[] = { 226, 150, 128 };

			if(graphing_lines_buffer[j] <= 8*i)
				*(graph_char_ptr++) = ' ';
			else if(graphing_lines_buffer[j] > (8*i + 7)) {
				blockcode[2] += 8;
				memcpy(graph_char_ptr, blockcode, 3);
				graph_char_ptr += 3;
			}
			else {
				blockcode[2] += graphing_lines_buffer[j] - 8*i;
				memcpy(graph_char_ptr, blockcode, 3);
				graph_char_ptr += 3;
			}
		}
		*graph_char_ptr = 0;

		log_puts_nowlock(graphing_char_buffer);
		log_puts_nowlock("\n");
	}

	log_puts_nowlock("ADC Graph: DONE\n");

	log_buffer_write_unlock();
}

struct sample_event_t {
		int32_t trigger_point;
		uint32_t trigger_tick;
		int16_t adc_is[2048];
		int16_t adc_exp[2048];
};
sample_event_t sample_buffer = {};

#define SAVG(n) selftest_calculations[n].ch_avg
#define SERR(n) selftest_calculations[n].ch_error
#define SSTD(n) int(selftest_calculations[n].ch_std_dev*1000)
#define PR_ALL(word) word(0), word(1), word(2), word(3), word(4)
void task() {
	task_handle = xTaskGetCurrentTaskHandle();

	LOGI("ADC", "ADC Thread starting up!");

	if(run_selftest())
		LOGI("ADC", "Selftest shows: ADC OK!");

	LOGI("ADC", "ADC averages are         %5d %5d %5d %5d %5d",
			PR_ALL(SAVG));
	LOGI("ADC", "ADC errors  are:         %5d %5d %5d %5d %5d",
			PR_ALL(SERR));
	LOGI("ADC", "ADC Std. Deviations are: %5d %5d %5d %5d %5d",
			PR_ALL(SSTD));

	// We get some weird transients at the start, so let the system settle first 
	// before restarting it
	vTaskDelay(200);
	Sampler::restart_sampler();

	while(1) {
		ulTaskNotifyTake(1, 100/portTICK_PERIOD_MS);

		if(Sampler::sample_available()) {
			// LOGI("ADC", "Sample became available!");
			// graph_sample();

			last_event_tick = xTaskGetTickCount();

			sample_buffer.trigger_tick = Sampler::sample_buffer_trigger_tick;
			sample_buffer.trigger_point = (xTaskGetTickCount() - Sampler::arc_end_tick) < 20 ? 1 : -1;

			// This function unrolls the circular buffer into a more sensible sample format,
			// which can then be sent off to the database
			for(int i=0; i<2048; i++) {
				auto s = Sampler::sample_buffer[(Sampler::sample_buffer_end - i) & 2047];
				sample_buffer.adc_is[i] = s.adc_is;
				sample_buffer.adc_exp[i] = s.adc_exp;
			}
			Sampler::restart_sampler();

			log_dump_data_base64("ADC/EVT", &sample_buffer, sizeof(sample_buffer));
		}

		if(Sampler::arc_detect_flag && ((xTaskGetTickCount() - Sampler::arc_end_tick) > 5)) {
			log_printf("ADC/ARC_TIMING: %lu %lu\n", Sampler::arc_start_tick, Sampler::arc_end_tick);
			Sampler::arc_detect_flag = false;
		}

		bool selftest_overdue = ( xTaskGetTickCount() ) - last_selftest > 3000;

		// Avoid selftests during runs. Selftests may mess with arc detection etc.
		bool selftest_allowed = !(SYS::LWL::has_link() && (SYS::LWL::current_power > 0));

		if(selftest_overdue && selftest_allowed) {
			run_selftest();

			last_selftest = xTaskGetTickCount();
		}

		test_data_buffer.send_logs();
	}
}

void trigger_task_from_isr() {
	if(SYS::ADCTask::task_handle) {
		int32_t woken = 0;
		vTaskNotifyGiveFromISR(SYS::ADCTask::task_handle, &woken);
		if(woken)
			portYIELD();
	}
}

void adc_zeroing_tick() {
	static int32_t extrasmooth_adc_zero_adjust = 0;

	if(!SYS::LWL::has_link() || (SYS::LWL::current_power == 0)) {
		// Since we use adc_smoothed, which is already taking the zeroing into account,
		// adc_smoothed will tend towards 0 once zero_adjust reaches the right value
		extrasmooth_adc_zero_adjust += (Sampler::adc_smoothed>>16);

		Sampler::adc_zero_adjust = extrasmooth_adc_zero_adjust >> 16;
	}
}

void tick_hook() {
	adc_zeroing_tick();

	// If the arc that was being detected has not been detected for 5 ticks, 
	// we can assume it has finished, and will trigger the ADC Task to send info to the server
	if(Sampler::arc_detect_flag && ((xTaskGetTickCount() - Sampler::arc_end_tick) > 5))
		vTaskNotifyGiveFromISR(task_handle, nullptr);

	auto current_tick = xTaskGetTickCountFromISR();
	adc_meas_tick = current_tick;

	// Delayed values. Since we only save the *last* point when our *current* one
	// diverges meaningfully (since we want the segment prior to the deviation looking good), 
	// we need to store them for one tick and save them later, if needed
	static int32_t last_smoothed_adc = 0;
	static int32_t last_expected_adc = 0;
	static uint8_t last_lwl_power    = 0;
	static int32_t last_noise_level  = 0;

	// Internal smoothing values. Act like a linear regression of the ADC data
	static int32_t last_sloped_smoothed_adc = 0;
	static int32_t smoothed_adc_delta = 0;

	// Timer value used to ensure that at least one point every five seconds is sent
	static TickType_t last_sent_tick = xTaskGetTickCountFromISR();

	int32_t adc_smoothed = (Sampler::adc_smoothed >> 16) + Sampler::adc_zero_adjust;

	int32_t current_adc_delta = adc_smoothed - last_smoothed_adc;

	// Adjust threshold so that linear segments get filtered out - they are boring since our graph is also linear-ish
	last_sloped_smoothed_adc += smoothed_adc_delta >> 4;

	auto adc_diff = adc_smoothed - (last_sloped_smoothed_adc>>4);

	if((adc_diff > 15) || (adc_diff < -15) || ((current_tick-last_sent_tick) > 5000)) {
		if(current_tick > 500) {
			//test_data_buffer.feed({int16_t(last_smoothed_adc), power_prop}, current_tick-1);
			test_data_buffer.feed({int16_t(last_smoothed_adc), int16_t(last_expected_adc), last_lwl_power, int16_t(last_noise_level)}, current_tick-1);
		}

		// Reset our threshold so we don't trigger on this value again
		last_sloped_smoothed_adc = adc_smoothed << 4;

		if((adc_diff > 15) || (adc_diff < -15))
			// Adjust our slope threshold values to face towards the current ADC slope. Not optimal, but will catch more details :>
			smoothed_adc_delta = (7*smoothed_adc_delta + (current_adc_delta<<8)) / 8;

		last_sent_tick = current_tick;
	}

	last_smoothed_adc = adc_smoothed;
	last_expected_adc = Sampler::power_detect_expected_adc + Sampler::adc_zero_adjust;
	last_lwl_power    = SYS::LWL::current_power;
	last_noise_level  = Sampler::adc_noise >> 8;
}

bool hw_ok() {
	return hw_is_ok;
}

}
}

extern "C" void adc_task_fn(void *args) {
	SYS::ADCTask::task();
}
extern "C" void adc_tick_hook(void) { SYS::ADCTask::tick_hook(); }
