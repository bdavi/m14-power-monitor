/**
 * @file adc_loop.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-05-11
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "adc_loop.h"

#define CCM __attribute__((section("ccmram")))
namespace ADC_LOOP {

// Flags for the RTOS-Side code to trigger switching MUX and measuring other channels, 
// such as internal calibration channels
CCM uint32_t one_shot_channel_selections = 0;
CCM uint32_t one_shot_channel_complete_flags = 0;

CCM uint32_t one_shot_channel_results[10] = {};

CCM uint32_t measurement_accumulator = 0;
CCM uint32_t measurement_acculuation_counter = 0;

CCM uint32_t current_mux_channel = 0;

CCM inline void core_tick() {

}

}