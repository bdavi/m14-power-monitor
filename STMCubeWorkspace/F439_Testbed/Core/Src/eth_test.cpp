/*
 * eth_test.cpp
 *
 *  Created on: May 17, 2022
 *      Author: xaseiresh
 */

#include <main.h>

#include <FreeRTOS.h>
#include <task.h>

#include <all/log/log.h>

#include <all/indicator_led/led.h>

#include <all/spi/spi.h>

#include <all/w5500/w5500.h>

SPI::Master test_spi_tx;
SPI::Device test_spi_dev(test_spi_tx, &(GPIOD->ODR), 4);

W5500::Core w5500(test_spi_dev);
W5500::BaseSocket test_sock(w5500);

extern SPI_HandleTypeDef hspi2;

extern "C" {

int SEND_BUFFER_TO_TCP(const char * data, size_t len) {
	if(test_sock.dump_tx(data, len))
		return 0;

	return -1;
}

void spi1_done_fn(SPI_HandleTypeDef * spi) {
	test_spi_tx.task_notify_rw_done();
}
void spi1_send_rw(const void *tx, void *rx, size_t num) {
	uint8_t * tx_ptr = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(tx));
	uint8_t * rx_ptr = reinterpret_cast<uint8_t *>(rx);

	if(tx == nullptr && rx == nullptr) {
		test_spi_tx.task_notify_rw_done();
	}
	else if(tx == nullptr) {
		HAL_SPI_Receive_DMA(&hspi2, rx_ptr, num);
	}
	else if(rx == nullptr) {
		HAL_SPI_Transmit_DMA(&hspi2, tx_ptr, num);
	}
	else {
		HAL_SPI_TransmitReceive_DMA(&hspi2, tx_ptr, rx_ptr, num);
	}
}

void eth_task_fn(void *arg) {
	test_spi_tx.init();
	test_spi_tx.set_rw_function(spi1_send_rw);


	hspi2.TxRxCpltCallback = spi1_done_fn;
	hspi2.TxCpltCallback = spi1_done_fn;
	hspi2.RxCpltCallback = spi1_done_fn;

	uint8_t flush_data[1] = {};

	test_spi_dev.deassert_cs();

	HAL_SPI_TransmitReceive_DMA(&hspi2, flush_data, flush_data, 1);
	vTaskDelay(1);


	w5500.init();

	W5500::REGS::crb_address_config_t addr_config;
	W5500_ASSIGN_IP(addr_config.gateway, 172, 16, 32, 1);
	W5500_ASSIGN_IP(addr_config.source_ip, 10, 8, 192, 1);
	W5500_ASSIGN_IP(addr_config.subnet_mask, 0, 0, 0, 0);

	w5500.configure_address(addr_config);

	W5500::BaseSocket::socket_config_t sock_cfg = {};
	sock_cfg.mode = W5500::BaseSocket::UDP;
	sock_cfg.source_port = 1234;
	sock_cfg.dest_port   = 9999;

	W5500_ASSIGN_IP(sock_cfg.dest_ip, 172, 16, 32, 172);

	test_sock.start(sock_cfg);

	vTaskDelay(10);

	LOGW("SYS", "System restart complete!");

	while(true) {
		w5500.loop_isr();
	}
}

}
