/**
 * @file adc_loop.h
 * @author David Bailey (david.bailey@ipp.mpg.de)
 * @brief  Core ADC measurement and comparison loop for the Arc Detection
 * @version 0.1
 * @date 2022-05-11
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

#include <stdint.h>

#ifdef __cplusplus

#ifndef ADC_BUFFER_BANK_SIZE 
#define ADC_BUFFER_BANK_SIZE 8192
#endif 

namespace ADC_CORE {

/**
 * @brief Pointer to the measured ADC circular buffer.
 * @details This points to the core ADC buffer. It is comprised of a large, continous memory section, which
 *   is split into two circular buffers. When a trigger event occurs, the core ADC interrupt will switch
 *   between banks and save the last written position and bank to adc_buffer_trigger_position and 
 *   adc_buffer_trigger_bank. This gives the RTOS software side time to reprocess the raw measurement data
 *   into calibrated data, and transmit it to the database for storage.
 * 
 * 
 */
extern uint16_t * adc_circular_buffer;

extern uint16_t   adc_buffer_trigger_position;
extern uint16_t   adc_buffer_trigger_bank;

extern uint16_t   absolute_trigger_threshold_hard;
extern uint16_t   absolute_trigger_threshold_soft;

extern uint16_t   slope_trigger_threshold_hard;
extern uint16_t   slope_trigger_threshold_soft;

}

#endif