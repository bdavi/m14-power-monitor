/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern DMA_HandleTypeDef hdma_spi2_rx;
extern DMA_HandleTypeDef hdma_spi2_tx;
extern SPI_HandleTypeDef hspi2;
extern TIM_HandleTypeDef htim2;
extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart6;
extern TIM_HandleTypeDef htim10;

/* USER CODE BEGIN EV */
#pragma GCC push_options
#pragma GCC optimize("Ofast")

int32_t sequence_pos = 0;
int32_t sequence_end_pos = 0;

uint32_t adc_trigger_thresh = 16384>>1;
uint32_t adc_trigger_nothresh = 15000>>1;

volatile  uint16_t data_buffer[2048] = {};

volatile adc_sampler_state_t adc_sampler_state = ADC_START;

#pragma pack(1)
typedef struct {
	uint8_t chsel:4;
	uint8_t gsel:4;
	uint8_t cmd_type:8;
} pga_11x_packet_t;
#pragma pack(0)

pga_11x_packet_t pga_config = {
		.chsel = 4,
		.gsel = 0,
		.cmd_type = 0b00111010,
};

volatile uint16_t  adc_selftest_buffer[5*8] = {};
static const pga_11x_packet_t selftest_configs[] = {
		{ .chsel = 0, .gsel = 0, .cmd_type = 0b00111010 },	// Set to external VCal
		{ .chsel = 12, .gsel = 0, .cmd_type = 0b00111010 },	// To GND
		{ .chsel = 13, .gsel = 0, .cmd_type = 0b00111010 },	// To 90% VCal
		{ .chsel = 14, .gsel = 0, .cmd_type = 0b00111010 },	// 10% VCal
		{ .chsel = 15, .gsel = 0, .cmd_type = 0b00111010 },	// External VRef ( diff. 0 )
};

volatile nodelay_lwl_usart_flags_t nodelay_usart_flags = {};

volatile uint32_t sent_ping_cnt = 0;
volatile uint32_t received_ping_cnt = 0;

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  while (1)
  {
  }
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles DMA1 stream3 global interrupt.
  */
void DMA1_Stream3_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream3_IRQn 0 */

  /* USER CODE END DMA1_Stream3_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_spi2_rx);
  /* USER CODE BEGIN DMA1_Stream3_IRQn 1 */

  /* USER CODE END DMA1_Stream3_IRQn 1 */
}

/**
  * @brief This function handles DMA1 stream4 global interrupt.
  */
void DMA1_Stream4_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream4_IRQn 0 */

  /* USER CODE END DMA1_Stream4_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_spi2_tx);
  /* USER CODE BEGIN DMA1_Stream4_IRQn 1 */

  /* USER CODE END DMA1_Stream4_IRQn 1 */
}

/**
  * @brief This function handles TIM1 update interrupt and TIM10 global interrupt.
  */
void TIM1_UP_TIM10_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_UP_TIM10_IRQn 0 */
	nodelay_usart_flags.keepalive = 1;
	USART6->CR1 |= USART_CR1_TXEIE_Msk;

  /* USER CODE END TIM1_UP_TIM10_IRQn 0 */
  HAL_TIM_IRQHandler(&htim10);
  /* USER CODE BEGIN TIM1_UP_TIM10_IRQn 1 */

  /* USER CODE END TIM1_UP_TIM10_IRQn 1 */
}

/**
  * @brief This function handles TIM2 global interrupt.
  */
void TIM2_IRQHandler(void)
{
  /* USER CODE BEGIN TIM2_IRQn 0 */

	// Clear interrupt flags
	TIM2->SR = 0;
	TIM2->CR1 |= TIM_CR1_CEN_Msk;

	uint32_t adc_data = SPI1->DR >> 1;
	SPI1->DR = 0;

	switch(adc_sampler_state) {
	case ADC_LOCKED: break;
	case ADC_SELFTEST_DONE: break;

	case ADC_SELFTEST_START:
		sequence_pos = 0;
		adc_sampler_state = ADC_SELFTEST;

	case ADC_SELFTEST: {
		uint8_t sequence_step = sequence_pos & 0xF;

		if(sequence_step == 0) {
			int8_t sequence_no = sequence_pos/16;

			SPI3->DR = *((uint16_t*)&selftest_configs[sequence_no]);
			TIM2->CCMR1 = 0b100 << TIM_CCMR1_OC1M_Pos;
		}
		else if(sequence_step == 3) {
			// Trigger OC1 to high on compare match
			TIM2->CCMR1 = 0b001 << TIM_CCMR1_OC1M_Pos;
		}
		else if(sequence_step >= 8) {
			adc_selftest_buffer[(sequence_step & 0x7) + ((sequence_pos & 0xF0)>>1)] = adc_data;
		}

		sequence_pos++;

		if(sequence_pos>>4 >= (sizeof(selftest_configs)/2))
			adc_sampler_state = ADC_SELFTEST_DONE;
	break;
	}

	case ADC_START:
		SPI3->DR = *((uint16_t*)&pga_config);
		TIM2->CCMR1 = 0b100 << TIM_CCMR1_OC1M_Pos;
		sequence_pos = 0;
		adc_sampler_state = ADC_MUX_SEL;
		break;
	case ADC_MUX_SEL:
		if(sequence_pos++ == 3) {
			// Trigger OC1 to high on compare match
			TIM2->CCMR1 = 0b001 << TIM_CCMR1_OC1M_Pos;

			sequence_pos = 0;
			adc_sampler_state = ADC_PRESAMPLING;
		}
	break;
	case ADC_PRESAMPLING:
		data_buffer[sequence_pos++] = adc_data;
		if(sequence_pos >= 256) {

			adc_sampler_state = ADC_WAIT_ON_NO_TRIGGER;

			if(adc_data > adc_trigger_nothresh)
				adc_sampler_state = ADC_WAIT_ON_TRIGGER;
		}
	break;
	case ADC_WAIT_ON_NO_TRIGGER:
		data_buffer[sequence_pos++] = adc_data;
		if(sequence_pos == 1024)
			sequence_pos = 0;

		if(adc_data > adc_trigger_nothresh)
			adc_sampler_state = ADC_WAIT_ON_TRIGGER;
	break;

	case ADC_WAIT_ON_TRIGGER:
		data_buffer[sequence_pos++] = adc_data;
		if(sequence_pos == 1024)
			sequence_pos = 0;

		if(adc_data < adc_trigger_thresh) {
			sequence_end_pos = (sequence_pos + 256) & 1023;
			adc_sampler_state = ADC_RECORDING;

			nodelay_usart_flags.arc_detected = 16;

			USART6->CR1 |= USART_CR1_TXEIE_Msk;
		}
	break;

	case ADC_RECORDING:
		data_buffer[sequence_pos++] = adc_data;
		if(sequence_pos == 1024)
			sequence_pos = 0;
		if(sequence_pos == sequence_end_pos)
			adc_sampler_state = ADC_LOCKED;
	break;
	}

  /* USER CODE END TIM2_IRQn 0 */
  /* USER CODE BEGIN TIM2_IRQn 1 */
  /* USER CODE END TIM2_IRQn 1 */
}

/**
  * @brief This function handles SPI2 global interrupt.
  */
void SPI2_IRQHandler(void)
{
  /* USER CODE BEGIN SPI2_IRQn 0 */

  /* USER CODE END SPI2_IRQn 0 */
  HAL_SPI_IRQHandler(&hspi2);
  /* USER CODE BEGIN SPI2_IRQn 1 */

  /* USER CODE END SPI2_IRQn 1 */
}

/**
  * @brief This function handles USART3 global interrupt.
  */
void USART3_IRQHandler(void)
{
  /* USER CODE BEGIN USART3_IRQn 0 */

  /* USER CODE END USART3_IRQn 0 */
  HAL_UART_IRQHandler(&huart3);
  /* USER CODE BEGIN USART3_IRQn 1 */

  /* USER CODE END USART3_IRQn 1 */
}

/**
  * @brief This function handles USART6 global interrupt.
  */
void USART6_IRQHandler(void)
{
  /* USER CODE BEGIN USART6_IRQn 0 */

	uint8_t usart_sr = USART6->SR;
	uint8_t usart_data = USART6->DR;

	if(usart_sr & USART_SR_RXNE_Msk) {

		switch(usart_data & 0xF) {
		default: break;

		case 9: nodelay_usart_flags.received_keepalive++; break;
		case 8: nodelay_usart_flags.received_loopback++;  break;
		}
	}

	if((usart_sr & USART_SR_TXE_Msk) && (USART6->CR1 & (USART_CR1_TXEIE_Msk))) {
		if(nodelay_usart_flags.arc_detected) {
			USART6->DR = (nodelay_usart_flags.arc_detected & 1) ? 170 : 0;
			nodelay_usart_flags.arc_detected--;
		}
		else if(nodelay_usart_flags.keepalive) {
			USART6->DR = 0x8
					| (nodelay_usart_flags.received_keepalive == nodelay_usart_flags.sent_keepalive) << 4;
			nodelay_usart_flags.keepalive = 0;
			nodelay_usart_flags.sent_keepalive++;
		}
		else
			USART6->CR1 &= ~(USART_CR1_TXEIE_Msk);
	}

  /* USER CODE END USART6_IRQn 0 */
  /* USER CODE BEGIN USART6_IRQn 1 */

  /* USER CODE END USART6_IRQn 1 */
}

/* USER CODE BEGIN 1 */
#pragma GCC pop_options
/* USER CODE END 1 */
