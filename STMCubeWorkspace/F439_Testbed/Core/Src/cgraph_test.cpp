/*
 * cgraph_test.cpp
 *
 *  Created on: 5 May 2022
 *      Author: xaseiresh
 */


#include <all/component_graph/component_graph.h>
#include <all/log/log.h>

#include <all/indicator_led/led.h>

#include <FreeRTOS.h>
#include <task.h>

#include <main.h>

extern SPI_HandleTypeDef hspi4;

CGRAPH::Component graph_root("root");

CGRAPH::Component graph_downlink("DOWNLINK");

CGRAPH::Component test_cpu("CPU");


uint16_t CURRENT_DISP_DATA_A = 0;

uint16_t reordered_data[256] = {};

CGRAPH::Component adc_a("ADC1");
CGRAPH::Component adc_b("ADC2");


using namespace Indicators;

LED status_led;

void test_print_graph() {
	log_buffer_write_lock();

	log_printf_nowlock("\nLOG: Starting graph dump from tick [%09u]\n", xTaskGetTickCount());

	int depth = 0;

	auto ptr = &graph_root;

	while(ptr) {
		log_puts_nowlock(ptr->get_colorization());
		log_puts_nowlock("LOG: ");
		for(int i=depth; i>0; i--)
			log_puts_nowlock(" |");

		log_printf_nowlock("-%s (state %s): %s\n", ptr->tag, ptr->get_state_name(), ptr->get_state_message());
		log_puts_nowlock(STR_COLOR_RESET);

		auto next_ptr = ptr->get_child_component();
		if(next_ptr) {
			depth++;
			ptr = next_ptr;
		}
		else {
			next_ptr = ptr->get_next();
			if(next_ptr) {
				ptr = next_ptr;
			}
			else {
				depth--;
				ptr = ptr->get_parent();
				if(ptr == nullptr)
					break;

				ptr = ptr->get_next();
			}
		}
	}

	log_puts_nowlock("LOG: DONE\n\n");

	log_buffer_write_unlock();
}

bool is_loopback = false;

bool perform_fiberoptic_check() {
	bool has_error = nodelay_usart_flags.sent_keepalive > (1+nodelay_usart_flags.received_keepalive);
	is_loopback = nodelay_usart_flags.sent_keepalive < (1+nodelay_usart_flags.received_loopback);

	nodelay_usart_flags.sent_keepalive = 0;
	nodelay_usart_flags.received_keepalive = 0;
	nodelay_usart_flags.received_loopback  = 0;

	if(is_loopback) {
		graph_downlink.set_state(CGRAPH::NOMINAL, "Downlink Loopback");
		return true;
	}
	else if(has_error) {
		graph_downlink.set_state(CGRAPH::FAILED, "No downlink present!");
		return false;
	}
	else {
		graph_downlink.set_state(CGRAPH::NOMINAL, "Downlink OK");

		return true;
	}
}

extern "C" {
void StartArcReport(void *arg) {
	while(1)
		vTaskDelay(1000);
}

void cgraph_test() {
	CGRAPH::init();

	graph_downlink.attach_to(graph_root);

	graph_downlink.set_parent_relevancy(CGRAPH::FAILING);

	test_print_graph();

	vTaskDelay(300);

	uint8_t test_led_data[13] = {};


	static TickType_t last_sampling_tick = xTaskGetTickCount();
	static TickType_t last_selftest_tick = xTaskGetTickCount() - 25000/portTICK_PERIOD_MS;

	adc_sampler_state = ADC_START;

	USART6->CR1 |= USART_CR1_RE_Msk | USART_CR1_RXNEIE_Msk;

	while(true) {
		// GPIOB->ODR ^= 1<<14;

		vTaskDelay(50);

		IWDG->KR = 0x0000AAAAu;


		if((adc_sampler_state == ADC_LOCKED) && ((xTaskGetTickCount() - last_sampling_tick) > 20)) {
			last_sampling_tick = xTaskGetTickCount();

			for(int j = 0; j<256; j++) {
				reordered_data[j] = data_buffer[(sequence_end_pos - 512 + 127 + j) & 1023];
			}
			log_dump_data_base64("ADC_SAMPLE", reordered_data, sizeof(reordered_data));

			nodelay_usart_flags.arc_counter = 0;
			adc_sampler_state = ADC_START;
		}

		if((xTaskGetTickCount() - last_selftest_tick) > 30000/portTICK_PERIOD_MS) {
			last_selftest_tick = xTaskGetTickCount();

			adc_sampler_state = ADC_SELFTEST_START;

			while(adc_sampler_state != ADC_SELFTEST_DONE)
				vTaskDelay(1);
			adc_sampler_state = ADC_START;

			uint32_t sums[5] = {};
			for(int i=0; i<5*8; i++) {
				sums[i >> 3] += uint32_t(adc_selftest_buffer[i]);
			}

			for(int i=0; i<5; i++) {
				sums[i] /= 8;
			}

			LOGI("ADC/SELFTEST", "Measured channel values are: %06d %06d %06d %06d %06d",
					sums[0], sums[1], sums[2], sums[3], sums[4]);
		}

		if(!perform_fiberoptic_check()) {
			status_led.mode = Indicators::WARN;
			status_led = Indicators::color_t::from_u32(Material::AMBER);
		}
		else if(xTaskGetTickCount() < 3000) {
			status_led.mode = RAINBOW;
		}
		else if((xTaskGetTickCount() - last_sampling_tick) < 750) {
			status_led.mode = Indicators::FLICKER;
			status_led = Indicators::color_t::from_u32(Material::GREEN);
		}
		else if(is_loopback) {
			status_led.mode = Indicators::STANDBY;
			status_led = Indicators::color_t::from_u32(Material::PURPLE);
		}
		else {
			status_led.mode = Indicators::STANDBY;
			status_led = Indicators::color_t::from_u32(Material::GREEN);
		}

		status_led.draw_tick();
		status_led.write_rgb_data_to(test_led_data);

		HAL_SPI_Transmit(&hspi4, test_led_data, 13, HAL_MAX_DELAY);
	}
}
}
