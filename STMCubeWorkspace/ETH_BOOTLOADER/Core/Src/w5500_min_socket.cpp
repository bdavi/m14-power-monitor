/*
 * w5500_min_socket.cpp
 *
 *  Created on: Sep 20, 2022
 *      Author: xaseiresh
 */

#include "main.h"

#include "w5500_min_socket.h"
#include "w5500_min.h"

#define W5500_EXP_IP(ip) ip[0], ip[1], ip[2], ip[3]

namespace W5500 {

using namespace REGS;

int Socket::sock_no_counter = 0;

Socket::Socket()
	: sock_no(-1),
	  connected(false),
	  restart_after(0),
	  current_config(),
	  on_reconnect(nullptr) {
}

void Socket::write_mr(sock_offsets reg, const void * tx_buffer, size_t num) {
	if(sock_no < 0)
		return;

	W5500::write(1, sock_no, reg, tx_buffer, num);
}
void Socket::read_mr(sock_offsets reg, void * rx_buffer, size_t num) {
	if(sock_no < 0)
		return;

	W5500::read(1, sock_no, reg, rx_buffer, num);
}

void Socket::write_u16(sock_offsets reg, uint16_t port) {
	port = ((port & 0xFF) << 8) | ((port & 0xFF00) >> 8);
	write_mr(reg, &port, 2);
}
uint16_t Socket::read_u16(sock_offsets reg) {
	uint16_t out = 0;
	read_mr(reg, &out, 2);

	return ((out & 0xFF) << 8) | ((out & 0xFF00) >> 8);
}

void Socket::reopen_socket() {
	if(sock_no == -1) {
		restart_after = 0;
		return;
	}

	uint8_t cmd = SOCK_CR_OPEN;
	write_mr(SOCK_COMMAND, &cmd, 1);
	if(current_config.mode == TCP_SERVER) {
		cmd = SOCK_CR_LISTEN;
		write_mr(SOCK_COMMAND, &cmd, 1);
	}
	else if(current_config.mode == TCP_CLIENT) {
		cmd = SOCK_CR_CONNECT;
		write_mr(SOCK_COMMAND, &cmd, 1);
	}
}

void Socket::start_tcp_server() {
	LOGB("W5500", "Starting TCP server on socket no. %d, port %d!", sock_no, current_config.source_port);

	sock_mode_register_t mr = {};
	mr.mode = 1;
	mr.nodelay_multicast = 1;
	write_mr(SOCK_MODE, &mr, 1);

	write_u16(SOCK_SOURCE_PORT, current_config.source_port);

	uint8_t cmd;
	cmd = 1;
	write_mr(SOCK_KEEPALIVE, &cmd, 1);

	cmd = SOCK_CR_OPEN;
	write_mr(SOCK_COMMAND, &cmd, 1);

	HAL_Delay(10);

	sock_status_type status;
	read_mr(SOCK_STATUS, &status, 1);

	if(status != SOCK_INIT) {
		LOGB("W5500", "Socket did not initialize!");
		sock_no = -1;
		return;
	}

	cmd = SOCK_CR_LISTEN;
	write_mr(SOCK_COMMAND, &cmd, 1);

	HAL_Delay(10);

	read_mr(SOCK_STATUS, &status, 1);

	if((status == SOCK_LISTEN)) {
		LOGB("W5500", "TCP Server on port %d opened!", current_config.source_port);

		return;
	}

	LOGB("W5500", "TCP Server on port %d failed to open!", current_config.source_port);
	sock_no = -1;
}

void Socket::start_tcp_client() {
	auto & t_ip = current_config.dest_ip;
	LOGB("W5500", "Starting TCP client connected to %d.%d.%d.%d:%d",
			t_ip[0], t_ip[1], t_ip[2], t_ip[3], current_config.dest_port);

	sock_mode_register_t mr = {};
	mr.mode = 1;
	write_mr(SOCK_MODE, &mr, 1);

	write_u16(SOCK_SOURCE_PORT, current_config.source_port);
	write_u16(SOCK_DEST_PORT, current_config.dest_port);

	write_mr(SOCK_DEST_IP, current_config.dest_ip, 4);

	uint8_t cmd;
	cmd = 1;
	write_mr(SOCK_KEEPALIVE, &cmd, 1);

	cmd = SOCK_CR_OPEN;
	write_mr(SOCK_COMMAND, &cmd, 1);
	HAL_Delay(2);

	sock_status_type status;
	read_mr(SOCK_STATUS, &status, 1);

	if(status != SOCK_INIT) {
		sock_no = -1;
		LOGB("W5500", "Socket failed to init!");

		return;
	}

	cmd = SOCK_CR_CONNECT;
	write_mr(SOCK_COMMAND, &cmd, 1);

	restart_after = HAL_GetTick() + 3000;
}

bool Socket::is_connected() {
	return connected;
}

void Socket::start(const socket_config_t & config) {
	current_config = config;

	sock_no = sock_no_counter++;

	LOGB("W5500", "Taking socket no. %d", sock_no);

	switch(config.mode) {
	case TCP_SERVER:
		start_tcp_server();
	break;

	case TCP_CLIENT:
		start_tcp_client();
	break;

	default:
		LOGB("W5500", "Unimplemented socket protocol chosen!");
	break;
	}
}

void Socket::tick() {
	sock_isr_t isr;
	read_mr(SOCK_ISR, &isr, 1);
	write_mr(SOCK_ISR, &isr, 1);

	if(current_config.mode == TCP_CLIENT) {
		if(isr.con) {
			connected = true;

			if(on_reconnect)
				on_reconnect();

			LOGB("W5500", "Connected to %d.%d.%d.%d:%d",
					W5500_EXP_IP(current_config.dest_ip), current_config.dest_port);

			restart_after = 0;
		}
		if(isr.timeout) {
			connected = false;
			LOGB("W5500", "Timeout from server %d.%d.%d.%d:%d",
					W5500_EXP_IP(current_config.dest_ip), current_config.dest_port);

			restart_after = HAL_GetTick() + 1000;
		}
		if(isr.discon) {
			if(connected) {
				LOGB("W5500", "Disconnect from server %d.%d.%d.%d:%d",
						W5500_EXP_IP(current_config.dest_ip), current_config.dest_port);
			}

			connected = false;

			restart_after = HAL_GetTick() + 250;
		}
	}

	if(current_config.mode == TCP_SERVER) {
		if(isr.con) {
			connected = true;

			if(on_reconnect)
				on_reconnect();

			auto & dest_ip = current_config.dest_ip;

			read_mr(SOCK_DEST_IP, &dest_ip, 4);

			LOGB("W5500", "Client %d.%d.%d.%d connected to port %d!",
					dest_ip[0], dest_ip[1], dest_ip[2], dest_ip[3],
					current_config.source_port);
		}

		if(isr.timeout || isr.discon) {
			connected = false;
			LOGB("W5500", "Client disconnected from port %d, re-opening.", current_config.source_port);

			reopen_socket();
		}
	}

	if((restart_after != 0) && (HAL_GetTick() > restart_after)) {
		reopen_socket();
	}
}

void Socket::read(void * data, size_t & size) {
	if((data == nullptr) || !connected) {
		size = 0;
		return;
	}

	size_t read_avail = read_u16(SOCK_RX_SIZE);

	if(size < read_avail)
		read_avail = size;

	size = read_avail;
	if(read_avail == 0) {
		return;
	}

	uint16_t read_ptr = read_u16(SOCK_RX_READ_PTR);
	W5500::read(3, sock_no, read_ptr, data, size);
	read_ptr += read_avail;
	write_u16(SOCK_RX_READ_PTR, read_ptr);
	uint8_t cmd = SOCK_CR_RECV;
	write_mr(SOCK_COMMAND, &cmd, 1);
}
void Socket::write(const void * data, size_t num) {
	// We're gonna do this real simple, send 1kB chunks using add_tx and send(),
	// nothing more. Hope you don't need any framing :P

	auto r_data_ptr = reinterpret_cast<const uint8_t*>(data);

	while(num) {
		if(!is_connected())
			return;

		size_t chunk_len = 512;
		if(num < chunk_len)
			chunk_len = num;

		while(chunk_len > read_u16(SOCK_TX_FREE_SIZE)) {
			HAL_Delay(1);
		}

		uint16_t write_head = read_u16(SOCK_TX_WRITE_PTR);
		W5500::write(2, sock_no, write_head, r_data_ptr, chunk_len);
		write_head += chunk_len;
		write_u16(SOCK_TX_WRITE_PTR, write_head);

		uint8_t cmd = SOCK_CR_SEND;
		write_mr(SOCK_COMMAND, &cmd, 1);

		r_data_ptr += chunk_len;
		num  -= chunk_len;
	}

	return;
}

}
