/*
 * device_config.cpp
 *
 *  Created on: 21 Sep 2022
 *      Author: xaseiresh
 */

#include "config_data.h"
#include "main.h"

#include "flash.h"

#include <cstring>

device_config_t device_cfg;
program_config_t program_cfg;

static const device_config_t fallback_config = {
		DEVICE_CONFIG_MAGIC_1,
		"FALLBACK-DEV",
		"c12d19fcca80",
		"testing",
		{
				{ 172, 16, 32, 1 },
				{ 255, 255, 255, 0 },
				{ 0x40, 0x12, 0x34, 0x56, 0x78, 0x9A },
				{ 172, 16, 32, 155 }
		},
		{ 172, 16, 32, 1 },
		9947,
		DEVICE_CONFIG_MAGIC_2
};

void load_device_config() {
	memcpy(&program_cfg, reinterpret_cast<const uint8_t*>(PROGRAM_CONFIG_LOCATION), sizeof(program_cfg));

	if(program_cfg._magic_1 != DEVICE_CONFIG_MAGIC_1
			|| program_cfg._magic_2 != DEVICE_CONFIG_MAGIC_2) {
		LOGB("CFG", "No program config found, assuming program invalid!");
	}

	memcpy(&device_cfg, reinterpret_cast<const uint8_t*>(DEVICE_CONFIG_LOCATION), sizeof(device_cfg));

	if(device_cfg._magic_1 != DEVICE_CONFIG_MAGIC_1
			|| device_cfg._magic_2 != DEVICE_CONFIG_MAGIC_2) {

		LOGB("CFG", "No device config present!");
		LOGB("CFG", "Fallback configuration used...");

		memcpy(&device_cfg, &fallback_config, sizeof(device_config_t));

		return;
	}

	LOGB("CFG", "Config loaded! This is %s!", device_cfg.name);

}

void save_device_config() {
	Flash::FlashWriter config_writer;

	config_writer.unlock();

	config_writer.begin(DEVICE_CONFIG_LOCATION,
			DEVICE_CONFIG_LOCATION + sizeof(device_config_t));
	config_writer.append(DEVICE_CONFIG_LOCATION, &device_cfg, sizeof(device_config_t));

	config_writer.lock();
}

void save_program_config(program_config_t prog) {
	prog._magic_1 = DEVICE_CONFIG_MAGIC_1;
	prog._magic_2 = DEVICE_CONFIG_MAGIC_2;

	HAL_FLASH_Unlock();

	uint8_t * prog_ptr = reinterpret_cast<uint8_t*>(&prog);
	for(size_t i=0; i<sizeof(program_config_t); i++) {
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, PROGRAM_CONFIG_LOCATION+i, *prog_ptr);
		prog_ptr++;
	}

	HAL_FLASH_Lock();

	program_cfg = prog;
}

bool program_present() {
	return (program_cfg._magic_1 == DEVICE_CONFIG_MAGIC_1
			&& program_cfg._magic_2 == DEVICE_CONFIG_MAGIC_2);
}
