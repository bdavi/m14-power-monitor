/*
 * command_glue.cpp
 *
 *  Created on: Sep 20, 2022
 *      Author: xaseiresh
 */

#include "main.h"

#include "CommandHandler.h"

#include "w5500_min.h"
#include "w5500_min_socket.h"

#include "flash.h"

#include "config_data.h"

#include <cstring>

CommandBuffer eth_server_cmd;
CommandBuffer eth_client_cmd;

CommandBuffer usart_cmd;

uint32_t autorestart_time = 10000;

namespace W5500 {
extern Socket log_server;
extern Socket log_client;
}
extern "C" void w5500_puts(const char *);
extern "C" void usart_puts(const char *);

Flash::FlashWriter flash_writer;
uint8_t flash_write_buffer[1024] = {};

program_config_t flash_prog_config = {};

void on_server_reconnect() {
	char intro_buffer[256] = {};

	snprintf(intro_buffer, 255, "CONNECT NAME=%s TYPE=ArcDetect MODE=BOOTLOADER BRANCH=%s VERSION=%ld\n",
			device_cfg.name,
			device_cfg.branch,
			program_cfg.version_nr);

	W5500::log_server.write(intro_buffer, strlen(intro_buffer));
}

void on_client_reconnect() {
	char intro_buffer[256] = {};

	snprintf(intro_buffer, 255, "CONNECT NAME=%s TYPE=ArcDetect MODE=BOOTLOADER BRANCH=%s VERSION=%ld\n",
			device_cfg.name,
			device_cfg.branch,
			program_cfg.version_nr);

	W5500::log_client.write(intro_buffer, strlen(intro_buffer));
}

void print_ip_piece(const char * piece, const uint8_t * ip) {
	LOGB("CFG", "%s %d.%d.%d.%d", piece, ip[0], ip[1], ip[2], ip[3]);
}

void print_config() {
	LOGB("CFG", "DEVICE NAME: %s", device_cfg.name);

	LOGB("CFG", " --- ETH CONFIG");
	auto mac_ptr = device_cfg.own_addr_config.source_hw_address;
	LOGB("CFG", "MAC %02X:%02X:%02X:%02X:%02X:%02X",
		mac_ptr[0], mac_ptr[1], mac_ptr[2], mac_ptr[3], mac_ptr[4], mac_ptr[5]);
	print_ip_piece("IP", device_cfg.own_addr_config.source_ip);
	print_ip_piece("SUBNET", device_cfg.own_addr_config.subnet_mask);
	print_ip_piece("GATEWAY", device_cfg.own_addr_config.gateway);

	LOGB("CFG", "--- SERVER CONFIG");
	LOGB("CFG", "SERVER ADDR %d.%d.%d.%d:%d",
		device_cfg.main_server_ip[0], device_cfg.main_server_ip[1], device_cfg.main_server_ip[2], device_cfg.main_server_ip[3],
		device_cfg.main_server_port);
}

void set_ipcfg_item(CommandBuffer & cmd, uint8_t * ptr) {
	uint8_t target_ip[4] = {};

	for(int i=0; i<4; i++) {
		long next_addr = 0;
		if(!cmd.parse_long(i, next_addr)) {
			cmd.puts("Specify IP as xxx xxx xxx xxx!\n");
			return;
		}
		if(next_addr < 0 || next_addr > 255) {
			cmd.puts("Invalid range for IP address element!\n");
			return;
		}

		target_ip[i] = next_addr;
	}

	memcpy(ptr, target_ip, 4);
	cmd.puts("OK\n");
}

bool ip_commands(CommandBuffer & cmd) {
	if(cmd.cmd_equal("RESET") || cmd.cmd_equal("RESTART") || cmd.cmd_equal("REBOOT")) {
		NVIC_SystemReset();
	}
	else if(cmd.cmd_equal("SET_IP")) set_ipcfg_item(cmd, device_cfg.own_addr_config.source_ip);
	else if(cmd.cmd_equal("SET_GATEWAY")) set_ipcfg_item(cmd, device_cfg.own_addr_config.gateway);
	else if(cmd.cmd_equal("SET_SUBNET")) set_ipcfg_item(cmd, device_cfg.own_addr_config.subnet_mask);
	else if(cmd.cmd_equal("SET_SERVER")) set_ipcfg_item(cmd, device_cfg.main_server_ip);
	else if(cmd.cmd_equal("SET_SERVER_PORT")) {
		long port = -1;
		if(!cmd.parse_long(0, port)) {
			cmd.puts("Specify port!\n");
			return true;
		}
		if(port < 0 || port > 65536) {
			cmd.puts("Port out of range! (0<p<65536)\n");
			return true;
		}

		device_cfg.main_server_port = port;
		cmd.puts("OK\n");
	}
	else if(cmd.cmd_equal("SET_MAC")) {
		uint8_t target_mac[6] = {};

		for(int i=0; i<6; i++) {
			long next_addr = 0;
			if(!cmd.parse_long(i, next_addr, 16)) {
				cmd.puts("Specify MAC as xxx xxx xxx xxx xxx xxx!\n");
				return true;
			}
			if(next_addr < 0 || next_addr > 255) {
				cmd.puts("Invalid range for IP address element!\n");
				return true;
			}

			target_mac[i] = next_addr;
		}

		memcpy(device_cfg.own_addr_config.source_hw_address, target_mac, 6);
		cmd.puts("OK\n");
	}
	else if(cmd.cmd_equal("SET_DEVICE_NAME")) {
		auto ptr = cmd.get_arg(0);
		if(ptr == nullptr)
			cmd.puts("No name given!\n");
		else if(strlen(ptr) > 16)
			cmd.puts("Name must be shorter than 15 characters!\n");
		else if(strchr(ptr, ' ') != nullptr) {
			cmd.puts("Name must not contain spaces!\n");
		}
		else {
			strncpy(device_cfg.name, cmd.get_arg(0), 15);
			cmd.puts("OK\n");
		}
	}
	else if(cmd.cmd_equal("SAVE_CFG")) {
		save_device_config();
		cmd.puts("OK\n");
	}
	else if(cmd.cmd_equal("GET_CFG")) {
		cmd.puts("OK\n");
		print_config();
	}
	else
		return false;

	return true;
}

void command_process(CommandBuffer & data) {
	if(data.cmd_equal("FLASH_START")) {
		long int start_addr = 0;
		long int length = 0;

		if(!data.parse_long(0, start_addr)) {
			data.puts("NACK\n");
			return;
		}
		if(!data.parse_long(1, length)) {
			data.puts("NACK\n");
			return;
		}
		if(!data.parse_long(2, flash_prog_config.version_nr)) {
			data.puts("NACK\n");
			return;
		}

		if(start_addr < USER_APP_VECTOR_TABLE) {
			data.puts("NACK\n");
			return;
		}

		data.puts("ACK\n");

		flash_writer.unlock();
		flash_writer.begin(start_addr, start_addr + length);
	}
	else if(data.cmd_equal("FLASH_WRITE")) {
		long int start_addr = 0;
		if(!data.parse_long(0, start_addr)){
			data.puts("NACK\n");
			return;
		}
		size_t flash_write_size = 1024;
		if(!data.parse_hex(1, flash_write_buffer, flash_write_size)) {
			data.puts("NACK\n");
			return;
		}

		data.puts("ACK\n");
		flash_writer.append(start_addr, flash_write_buffer, flash_write_size);
	}
	else if(data.cmd_equal("FLASH_DONE")) {
		save_program_config(flash_prog_config);
		data.puts("OK\n");
	}
	else if(data.cmd_equal("LAUNCH_APP")) {
		data.puts("OK\n");

		flash_writer.lock();
		launch_app();
	}
	else if(ip_commands(data)) {}
	else
		data.puts("UNKNOWN CMD\n");
}

extern "C" void command_tick() {
	size_t transfer_size = 255;
	do {
		char transfer_buffer[256] = {};
		W5500::log_server.read(transfer_buffer, transfer_size);
		eth_server_cmd.feed(transfer_buffer);

	} while(transfer_size > 0);

	transfer_size = 255;
	do {
		char transfer_buffer[256] = {};
		W5500::log_client.read(transfer_buffer, transfer_size);
		eth_client_cmd.feed(transfer_buffer);

	} while(transfer_size > 0);

	if(USART3->SR & (USART_SR_RXNE_Msk)) {
		char u_data = USART3->DR;
		if(u_data == '\r' || u_data == '\n') {
			usart_puts("\n");
		}

		char data[2] = { char(u_data), 0};
		usart_cmd.feed(data);

		usart_puts("\x1b[2K> ");
		usart_puts(usart_cmd.get_buffer());
		usart_puts("\r");
	}

	if(W5500::log_server.is_connected())
		autorestart_time = HAL_GetTick() + 5000;
	if(W5500::log_client.is_connected())
		autorestart_time = HAL_GetTick() + 5000;

	if((HAL_GetTick() > autorestart_time) && program_present())
		launch_app();
}

extern "C" void command_init() {
	load_device_config();

	W5500::log_server.on_reconnect = on_server_reconnect;
	W5500::log_client.on_reconnect = on_client_reconnect;

	W5500::init();

	eth_server_cmd.cmd_puts_fn = w5500_puts;
	eth_server_cmd.cmd_exec_fn = command_process;

	eth_client_cmd.cmd_puts_fn = w5500_puts;
	eth_client_cmd.cmd_exec_fn = command_process;

	usart_cmd.cmd_exec_fn = command_process;
	usart_cmd.cmd_puts_fn = usart_puts;
}
