/*
 * w5500_min.c
 *
 *  Created on: 20 Sep 2022
 *      Author: xaseiresh
 */

#include "w5500_defs.h"
#include "main.h"

#include "w5500_min_socket.h"

#include <cstring>

#include "config_data.h"

#define W5500_SPI SPI5
#define W5500_CS_LOW()  GPIOD->ODR &= ~1
#define W5500_CS_HIGH() GPIOD->ODR |= 1

namespace W5500 {
using namespace REGS;

Socket log_server;
Socket log_client;

enum w5500_state_t {
	UNINITIALIZED,
	AWAITING_PHY,
	INITIALIZED
};

w5500_state_t chip_state = UNINITIALIZED;

uint8_t spi_tr(uint8_t data = 0) {
	while((W5500_SPI->SR & SPI_SR_TXE_Msk) == 0) {}
	W5500_SPI->DR = data;
	while((W5500_SPI->SR & SPI_SR_RXNE_Msk) == 0) {}
	return W5500_SPI->DR;
}

void spi_tx(const void * data_ptr, size_t num) {
	const uint8_t * data = reinterpret_cast<const uint8_t*>(data_ptr);

	while(num > 0) {
		spi_tr(*data);
		data++;
		num--;
	}
}
void spi_rx(void * rx_data, size_t num) {
	uint8_t * data = reinterpret_cast<uint8_t*>(rx_data);
	while(num > 0) {
		*data = spi_tr();
		data++;
		num--;
	}
}

void read(uint8_t reg_type, uint8_t sock_no, uint16_t offset, void * rx_buffer, size_t size) {
    spi_header_t spi_header = {};
    spi_header.addr_offset = ((offset & 0xFF) << 8) | ((offset & 0xFF00) >> 8);
    spi_header.opmode = 0;
    spi_header.rw = 0;
    spi_header.regtype = reg_type;
    spi_header.socket_no = sock_no;

    W5500_CS_LOW();

    spi_tx(&spi_header, sizeof(spi_header));
    spi_rx(rx_buffer, size);

    W5500_CS_HIGH();
}

void write(uint8_t reg_type, uint8_t sock_no, uint16_t offset, const void * data, size_t num) {
    spi_header_t spi_header = {};
    spi_header.addr_offset = ((offset & 0xFF) << 8) | ((offset & 0xFF00) >> 8);
    spi_header.opmode = 0;
    spi_header.rw = 1;
    spi_header.regtype = reg_type;
    spi_header.socket_no = sock_no;

    W5500_CS_LOW();

    spi_tx(&spi_header, sizeof(spi_header));
    spi_tx(data, num);

    W5500_CS_HIGH();
}

void read_crb(crb_offsets reg, void * rx_buffer, size_t num) {
    read(0, 0, reg, rx_buffer, num);
}
void write_crb(crb_offsets reg, const void * tx_buffer, size_t num) {
    write(0, 0, reg, tx_buffer, num);
}

void init() {
	LOGB("W5500", "Beginning minimal configuration...");

	W5500_CS_HIGH();
	W5500_SPI->CR1 |= SPI_CR1_SPE_Msk;

	spi_tr(0);

	HAL_Delay(2);

	crb_mode_t crb_mode = {};

	for(int i=0; i<2; i++) {
		// Perform software reset
		crb_mode.sw_reset = 1;
		write_crb(CRB_MODE, &crb_mode, 1);

		HAL_Delay(10);
	}



	uint8_t dummy_u8;
	read_crb(CRB_VERSIONR, &dummy_u8, 1);

	if(dummy_u8 != 0x04) {
		LOGB("W5500", "Version nr. did not read back as 0x04 (found: %x).", dummy_u8);
		LOGB("W5500", "Ethernet chip either not available or wrong version. Discontinuing.");

		return;
	}

	crb_mode.sw_reset = 0;
	crb_mode.wol_enable = 1;
	write_crb(CRB_MODE, &crb_mode, 1);

	dummy_u8 = 3;
	write_crb(CRB_RETRY_COUNT, &dummy_u8, 1);

	crb_phy_config_t phy_cfg = {};
	phy_cfg.opmode = 0b011;
	phy_cfg.opmode_configure = 1;

	write_crb(CRB_PHY_CONFIG, &phy_cfg, 1);

	phy_cfg.reset = 1;
	write_crb(CRB_PHY_CONFIG, &phy_cfg, 1);

	chip_state = AWAITING_PHY;

	LOGB("W5500", "Chip OK, waiting on phy...");
}

bool has_phy() {
    crb_phy_config_t phy_cfg = {};
    read_crb(CRB_PHY_CONFIG, &phy_cfg, 1);

    return phy_cfg.link_status;
}

void setup_ip(const REGS::crb_address_config_t & config) {

	write_crb(CRB_GATEWAY_ADDR, &config, sizeof(config));

	LOGB("W5500", "MAC is %02x:%02x:%02x:%02x:%02x:%02x",
		config.source_hw_address[0], config.source_hw_address[1], config.source_hw_address[2],
		config.source_hw_address[3], config.source_hw_address[4], config.source_hw_address[5]);

	LOGB("W5500", "IP Address configured to: %d.%d.%d.%d (Mask %d.%d.%d.%d)",
    config.source_ip[0], config.source_ip[1], config.source_ip[2], config.source_ip[3],
    config.subnet_mask[0], config.subnet_mask[1], config.subnet_mask[2], config.subnet_mask[3]);

	LOGB("W5500", "Gateway address is:  %d.%d.%d.%d",
	config.gateway[0], config.gateway[1], config.gateway[2], config.gateway[3]);

}

void tick() {
	switch(chip_state) {
	default: break;

	case AWAITING_PHY:
		if(has_phy()) {
			LOGB("W5500", "Phy connected!");

			chip_state = INITIALIZED;

			setup_ip(device_cfg.own_addr_config);

			log_server.start({
				TCP_SERVER,
				22, 0, {}
			});

			W5500::socket_config_t client_cfg = {
					TCP_CLIENT,
					39947,
					device_cfg.main_server_port,
					{}
			};

			memcpy(&client_cfg.dest_ip[0], &device_cfg.main_server_ip[0], 4);

			log_client.start(client_cfg);
		}
		break;
	}


	log_server.tick();
	log_client.tick();
}

}

extern "C" void w5500_init() { W5500::init(); }
extern "C" void w5500_tick() { W5500::tick(); }
extern "C" void w5500_puts(const char * str) {
	W5500::log_server.write(str, strlen(str));
	W5500::log_client.write(str, strlen(str));
}
