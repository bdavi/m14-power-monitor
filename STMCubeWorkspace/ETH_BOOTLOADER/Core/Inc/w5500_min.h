/*
 * w5500_min.h
 *
 *  Created on: Sep 20, 2022
 *      Author: xaseiresh
 */

#ifndef INC_W5500_MIN_H_
#define INC_W5500_MIN_H_

#include <stdint.h>
#include <stddef.h>

namespace W5500 {

void read(uint8_t reg_type, uint8_t sock_no, uint16_t offset, void * rx_buffer, size_t size);
void write(uint8_t reg_type, uint8_t sock_no, uint16_t offset, const void * data, size_t num);

void init();

}

#endif /* INC_W5500_MIN_H_ */
