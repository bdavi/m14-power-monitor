/*
 * w5500_min_socket.h
 *
 *  Created on: Sep 20, 2022
 *      Author: xaseiresh
 */

#ifndef INC_W5500_MIN_SOCKET_H_
#define INC_W5500_MIN_SOCKET_H_

#include "w5500_defs.h"

#include <stdint.h>
#include <stddef.h>

namespace W5500 {

enum sock_state_t {
	AWAITING_INIT,
	AWAITING_CONNECT,
	CONNECTED,
	PAUSED_DISCONNECTED
};

//! Socket mode enum, defines in what mode the socket will behave.
enum socket_mode_t {
	UNUSED,			//!< Socket is currently inactive and requires configuration.
	TCP_CLIENT,		//!< Socket is configured as TCP client.
	TCP_SERVER,		//!< Socket is configured as TCP server
	UDP				//!< Socket is configured as UDP socket for raw rw
};

/**
 * @brief User Socket configuration register.
 * @details This register is meant to let the user easily pass configuration about
 *   how this socket should behave. It is used for the initial BaseSocket.start call,
 *   to set up necessary parameters.
 */
struct socket_config_t {
	socket_mode_t mode;		//!< Mode for the socket to be in.
	uint16_t source_port;	//!< Source port. Unused for TCP Client, local socket for TCP Server and UDP mode.
	uint16_t dest_port;		//!< Destination port. Unused for TCP Server, required for TCP client and UDP mode.
	uint8_t  dest_ip[4];	//!< Destination IP to connect to. Unused for TCP server, required for TCP client and UDP.
};

class Socket {
private:
	static int sock_no_counter;
	int sock_no;

	bool connected;

	uint32_t restart_after;

	// Current configuration. Used when closing and re-opening the socket, when
	// a reconnect is necessary, etc.
	socket_config_t current_config;

	//! Wrapper function to quickly write to the socket Mode Register for config
	void write_mr(REGS::sock_offsets reg, const void * tx_buffer, size_t num);
	//! Wrapper function to quickly read from the socket mode register
	void read_mr(REGS::sock_offsets reg, void * rx_buffer, size_t num);

	//! Convenience function to write a u16 value to the Mode Register (endianness swap needed).
	void write_u16(REGS::sock_offsets, uint16_t data);
	//! Convenience function to read a u16 value from the Mode Register (with endianness swap)
	uint16_t read_u16(REGS::sock_offsets);

	void reopen_socket();

	void start_tcp_server();
	void start_tcp_client();

public:
	void (*on_reconnect)(void);

	bool is_connected();

	Socket();

	void start(const socket_config_t & config);

	void tick();

	void read(void * data, size_t & size);
	void write(const void * data, size_t size);
};

}

#endif /* INC_W5500_MIN_SOCKET_H_ */
