/*
 * config_data.h
 *
 *  Created on: 21 Sep 2022
 *      Author: xaseiresh
 */

#ifndef INC_CONFIG_DATA_H_
#define INC_CONFIG_DATA_H_

#include "w5500_defs.h"

#define DEVICE_CONFIG_LOCATION 0x0800c000
#define DEVICE_CONFIG_MAGIC_1  0x712BF58B
#define DEVICE_CONFIG_MAGIC_2  0xB85FB217

#define PROGRAM_CONFIG_LOCATION 0x08100000

struct device_config_t {
	uint32_t _magic_1;
	char name[16];
	char md5_shared_key[16];
	char branch[16];
	W5500::REGS::crb_address_config_t own_addr_config;
	uint8_t  main_server_ip[4];
	uint16_t main_server_port;
	uint32_t _magic_2;
};

struct program_config_t {
	uint32_t _magic_1;
	long int crc32;
	long int version_nr;
	uint32_t _magic_2;
};

extern device_config_t device_cfg;
extern program_config_t program_cfg;

void load_device_config();
void save_device_config();

void save_program_config(program_config_t prog);

bool program_present();

#endif /* INC_CONFIG_DATA_H_ */
