/*
 * CommandHandler.h
 *
 *  Created on: Sep 20, 2022
 *      Author: xaseiresh
 */

#ifndef SRC_COMMANDHANDLER_H_
#define SRC_COMMANDHANDLER_H_

#include <stddef.h>

class CommandBuffer {
private:
	int  cmd_buffer_ptr;
	char cmd_buffer[2048];

	const char * cmd_start_ptr;

	bool wait_for_cmd_end;

public:
	void (*cmd_puts_fn)(const char *);
	void (*cmd_exec_fn)(CommandBuffer &);

	CommandBuffer();

	const char *get_buffer();

	void puts(const char * c);

	void exec(const char *cmd);
	void feed(const char * data);

	bool cmd_equal(const char *name);

	const char * get_arg(int i);

	bool parse_long(int i, long int & data, int base = 0);
	bool parse_hex(int i, void * buffer, size_t & size);
};

#endif /* SRC_COMMANDHANDLER_H_ */
