/*
 * flash.h
 *
 *  Created on: Sep 16, 2022
 *      Author: xaseiresh
 */

#ifndef STM32_FLASH_FLASH_H_
#define STM32_FLASH_FLASH_H_

#include <stdint.h>
#include <stddef.h>

namespace Flash {

void get_sector_number(uint32_t location);

class FlashWriter {
private:
	uint32_t start_location;
	uint32_t continue_location;
	uint32_t end_location;

	static void write_optregs(uint32_t opt, uint32_t opt1);

public:
	FlashWriter();

	static int get_sector_number(uint32_t location);

	void unlock();
	void lock();

	bool begin(uint32_t start, uint32_t end);
	bool append(uint32_t location, const void * data, size_t num_bytes);

	bool check_crc(uint32_t computed_crc);
};

}

#endif /* STM32_FLASH_FLASH_H_ */
